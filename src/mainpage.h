/** @mainpage Rfid controller 
 * @authors Kévin Raymond for the LOR department (it-sudparis)
 *
 * @section intro Introduction
 * This package provides an rfid controller for use as research platform.
 * Its goal is to:
 *  - Efficiently manage a bunch of several rfid reader (manufacturer/technology independant);
 *  - Avoid conflicts implementing a clever manager, which would deactivate/activate some readers,
 *    set directionnal antennas… It all depends of your architecture;
 *  - Be a research platform easily expendable;
 *  - Be dynamic, it is designed to work even with multiple controller instance (in different machines). 
 *    If you like this possibility, consider implementing a controller hierarchy feature and using the tcpproxy providen (enhenced with TCP broadcast).
 *
 * @subsection Architecture:
 * \verbatim
 
   (TPC socket server)
      –––––––––––––––
      | Controller1 | <················· 
      –––––––––––––––                  :
             :                         :
             :                         :
             :..............           :
                           :           :
                           :           :
   (TPC socket server)     :   (TCP client/Server)        (TCP socket client)     
      –––––––––––––––      :       −−−−−−−−−−−             –––––––––––––––––
      | Controller2 | <····+····· | TCP Proxy |<·········· | Web interface |
      –––––––––––––––      :       −−−−−−−−−−−             –––––––––––––––––
            :              :                                       :
            :              :          ______                       :
            :              ·········>(      )                      :
            :                        (      )                      :
            ························>(  DB  )<······················
                                     (      )
                                     (______)
 
 \endverbatim
 *
 * @section install_sec Setup:
 *  - You need the database, somewhere in the wild;
 *  - Use the website provided somewhere to configure it and see logs (should be on the same network, 
 *    as it is using sockets and DB access;
 *  - Some readers and tags, obviously.
 *  
 * @section dev_sec Development:
 *  Mind that this documents only the controller, not the adminitrative web server code.
 *
 * @subsection INSTALLATION
 *   - read the README file
 *
 */
