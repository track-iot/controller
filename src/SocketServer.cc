/**  SocketServer class implementation
 *
 * \file @FILE@
 * #include "Socket.h"
 *
 */


#include "SocketServer.h"
#include "DBhandler.h"

using namespace std;

/**  SocketServer constructor
 *		
 *
 */
SocketServer::SocketServer(int port): localPort(port), srvSocket(-1), active_connection_nbr(0), running(false)
{
/* All is done by the Pthread class */
}

SocketServer::SocketServer(): localPort(DEFAULT_PORT), srvSocket(-1), active_connection_nbr(0), running(false)
{
/* All is done by the Pthread class */
}

/**  SocketServer destructor
 *		Call the CloseC() method
 *
 */
SocketServer::~SocketServer()
{
  running = false;
  if(srvSocket >= 0)
    close(srvSocket);
}

bool SocketServer::is_running()
{
  return running;
}

/**  SocketServer constructor
 *	close the socket
 *  \@todo check error
 *	@return true
 */
void SocketServer::disconnect_all_clients(Client* clients)
{
   broadcast_message(clients, "The server is closing, bye");

   for(int i = 0; i < active_connection_nbr; i++)
      close(clients[i].sock);
}


void SocketServer::broadcast_message(Client* clients, const char* buff)
{
   char toto[BUF_SIZE];
   std::stringstream log;
 
   strncpy(toto, buff, BUF_SIZE - 1);

   log << "Message brodcasted to " << active_connection_nbr << " clients-> " << toto;
   write_log(log.str());

   for(int i = 0; i < active_connection_nbr; i++)
       write_to_client(clients[i].sock, toto);
}


void SocketServer::broadcast_message(Client* clients, Client sender, const char* buff)
{
   std::stringstream log;

   static DBhandler dbhandlr = DBhandler::getInstance();

/** @TODO should initialize arrays? */

  log << "Sending msg to " << active_connection_nbr << " clients -> " << buff; 
  std::string tmp = log.str();
  dbhandlr.log(tmp.c_str(), INFO_TYPE); ///@TODO change that, I want either to send a std string or to use c string
  dbhandlr.logNotif(buff);

   for(int i = 0; i < active_connection_nbr; i++)
   {
      if(sender.sock != clients[i].sock) /* Don't send it to the sender */
        write_to_client(clients[i].sock, buff);
   }
}

bool SocketServer::init(void)
{
   int sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
   struct sockaddr_in sin = { 0 };
   active_connection_nbr = 0;

   if(sock < 0)
   {
      write_log("socket error in SocketServer", ERR_TYPE, errno);
      return false;
   }

   sin.sin_addr.s_addr = htonl(INADDR_ANY);
   sin.sin_port = htons(localPort);
   sin.sin_family = AF_INET;

   while(bind(sock,(struct sockaddr *) &sin, sizeof sin) < 0)
   {
      write_log("bind error in SocketServer", ERR_TYPE, errno);
      if(errno == EADDRINUSE) /// we hope the port to be released at some point. @ATTENTION this is fo test purpose.
       {
          sleep(2);
       }
      else
        return false;
   }


   if(listen(sock, MAX_CLIENTS) < 0)
   {
      write_log("listen error in SocketServer", ERR_TYPE, errno);
      return false;
   }

   srvSocket = sock;
   return true;
}

/** Remove one Client from the Client array
 *  Beware, it won't close the socket */
void SocketServer::remove_client(Client* clients, int i)
{
   memmove(clients + i, clients + i + 1, (active_connection_nbr - i - 1) * sizeof(Client));
   active_connection_nbr--; 
}


bool SocketServer::add_client(Client* clients, fd_set& rdfs, int i, int& max)
{

  if(active_connection_nbr + 1 > MAX_CLIENTS)
   {  
      write_log("TOO much open connections, consider incresing MAX_CLIENTS", ERR_TYPE);
      return false; 
   }

  char buffer[BUF_SIZE];
  char* mess;

  struct sockaddr_in csin = { 0 };
  unsigned int addr_len = sizeof(csin);
  int csock = accept(srvSocket, (struct sockaddr*) &csin, (socklen_t *) &addr_len);
  if(csock < 0)
   {
      write_log("accept error in SocketServer", ERR_TYPE, errno);
      return false;
   }
#if 0

         /* after connecting the client should send smth */
         if(read_client(csock, buffer) == -1)
         {
            /* disconnected */
            continue;
         }
#endif
//               broadcast_message(clients, client, buffer);
//                        /* what is the new maximum fd ? */
//
  max = csock > max ? csock : max;

  FD_SET(csock, &rdfs);  
  Client c = { csock };

  strncpy(c.ip, inet_ntoa(csin.sin_addr), IP_SIZE - 1);
  clients[active_connection_nbr] = c;

  if(DEBUG_SOCK > 5)
   {
      std::stringstream log;
      log << c.ip << "\t:" << i << ":" << c.sock << " sck #" << active_connection_nbr << " connected" ;
      write_log(log.str());
      log.str("");
   }
  active_connection_nbr++;
  return true;
}

void SocketServer::write_to_client(SOCKET sock, const char* buff)
{
   if(send(sock, buff, strlen(buff), 0) < 0)
   {
      write_log("send error in SocketServer", ERR_TYPE, errno);
   }
}

void SocketServer::send_answer(SOCKET sock, const char* buff)
{
  write_to_client(sock, buff);
}


int SocketServer::read_client(SOCKET sock, char* buff)
{
   int n;
   if((n = recv(sock, buff, BUF_SIZE - 1, 0)) < 0)
   {
      write_log("perror recv()", ERR_TYPE, errno);
      // error occured, disconnect the client
      n = 0;
   }
   buff[n] = 0;

   return n;
}

Sockmsg SocketServer::get_oldest_msg()
{
  Sockmsg msg;

  if(queue.empty()) 
    msg.cmd = "";
  else
   {
    msg = queue.front(); /** Get oldest entry */
    queue.pop(); /** Delete the oldest entry */
   }
  return msg;
}


/** Fill the queue with the message and its sender
 * filtering the username from the message, if username absent, forget
 * @params str in the form: <username>:<cmd>#<arg1>#<arg2>…
 * the username could not contain `#', and should finish by `:'.
 */
void SocketServer::q_push(std::string str, SOCKET sock)
{
  size_t pos;
  Sockmsg msg;

  pos = str.find_first_of(NAME_DELIM);
  if(pos < str.npos)
   {
      msg.name = str.substr(0, pos);
      if(msg.name.find_first_of(ARG_DELIM) == msg.name.npos)
       {
            msg.cmd = str.substr(++pos);
            msg.sock = sock;
            queue.push(msg);
       }
   }
}

/**  
 *		Implement the run() methode, which is threaded.
 *		We store all clients in an array
 *		if reveive a discovery message, forget about it
 *		don't send ACK message, but send back from the manager if the command was working
 *
 */
void SocketServer::run()
{
  char buffer[BUF_SIZE];
  char* mess;
  int i = 0;
  std::stringstream log;

  if(init() != true)
  {
    //Should add "killed=1" ?
    end();
  }

  int max = srvSocket;
  Client clients[MAX_CLIENTS];

  fd_set rdfs;

  running = true;
  log << "Socket server running on port: " << localPort;
  write_log(log.str());
  log.str("");

   while(running)
   {
      FD_ZERO(&rdfs);

      /* add the connection socket */
      FD_SET(srvSocket, &rdfs);

      /* add socket of each client */
      for(i = 0; i < active_connection_nbr; i++)
         FD_SET(clients[i].sock, &rdfs);

      /* Check if events occured. See http://stackoverflow.com/questions/7637765 */
      if(select(max + 1, &rdfs, NULL, NULL, NULL) == -1)
      {
         write_log("select error in SocketServer", ERR_TYPE, errno);
      }
      if(FD_ISSET(srvSocket, &rdfs)) /* new client ? */
      {
         add_client(clients, rdfs, i, max);
      }
      else  /* one client was talking */
      {
         int i = 0;
         for(i = 0; i < active_connection_nbr; i++)
         {
            if(FD_ISSET(clients[i].sock, &rdfs)) /* which client is it? */
            {
               Client client = clients[i];
               int c = read_client(clients[i].sock, buffer);
               if(c == 0)  /* client disconnected? */
               {
                  close(clients[i].sock);
                  remove_client(clients, i);
                  strncpy(buffer, client.ip, BUF_SIZE - strlen(buffer) -  1);

                  log << client.ip << "\t" << i << ":" << client.sock << " <- " << " disconnected !"; 
                  write_log(log.str());
                  log.str("");

                  continue;
               }

               mess = (char*)malloc((strlen(buffer)+1) * sizeof(char));
               if (mess == NULL)
                  perror("Malloc error");

               strcpy(mess, buffer);
               if(strcmp(mess, DISCOVERY_MSG) == 0)
                {
                  if(DEBUG_SOCK > 6) write_log("-- someone sending a discovering msg");
                }
               else if(strcmp(mess, CLOSE_SCK_MSG) == 0)
                {
                    close(clients[i].sock);
                    remove_client(clients, i);
                    if(DEBUG_SOCK > 6) write_log("-- someone sending a discovering:quit msg, closing socket");
                }
               else if(strcmp(mess, QUIT_MSG) == 0)
                { 
                   
                   q_push(mess, client.sock);  /** We add the message received to the queue */
                   write_log("-- Exiting", WARN_TYPE);
                   running = false;
                   sleep(2); /// @TODO check, let the DeviceManager getting this message!
                }
               else
                {
                  q_push(mess, client.sock);  /** We add the message received to the queue */
                  broadcast_message(clients, client, mess);
                }

            }
         }
      }

      usleep(1000*200);
			yield();	/// release proc

   } // while(running) 

   disconnect_all_clients(clients);
   close(srvSocket);
}



/* vim: set ts=2 sw=2 sts=2 et:*/
