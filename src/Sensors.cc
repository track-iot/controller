/**  Sensors class implementation
 *
 *
 *
 */


#include "Sensors.h"

using namespace::std;


bool Sensors_s::add_new(string node, string id)
{
  /* Check if could open the port */
  int fd = open(node.c_str(), O_RDWR | O_NOCTTY | O_NDELAY);
  if(fd == -1)
    return false;

///@TODO should check here if serial number, Productid and Vendorid match
// if(right_device(node.c_str(), idproduct, idvendor, serial) == 0)
//  return false

  close(fd);

  Sensors_s* s = new Sensors_s;
  if(s == NULL)
    write_log("new sensor NULL", ERR_TYPE);
  
/*  Sensors redbee(node);
    s->redbee = &redbee;
*/
  s->sensor = new Sensors(node, id);
  s->next = next;
  next = s;
  return true;
}

void Sensors_s::remove_next()
{
  write_log("Deleting sensor", WARN_TYPE);
  Sensors_s* s = next;
  if(s == NULL)
    write_log("new sensor NULL in rm", ERR_TYPE);
  next = s->next;
  delete s->sensor;
  delete s;
}

std::string Sensors::get_port()
{
  return port;
}

string Sensors::get_uid()
{
  return uid;
}


Sensors::Sensors(std::string node, string id): fd(NULL)
{
    uid = id;
    port = node;
    init();
}


Sensors::~Sensors()
{
      fclose(fd);
    if(DEBUG > 1)
        write_log("Sensors destructor");
}

const char* Sensors::process_response(const char* response, const char* type)
{
  return "please implement me";
}


void Sensors::init()
{
  fd = fopen(port.c_str(), "r+");
  if(fd == NULL)
    write_log("COULD NOT OPEN PORT", ERR_TYPE);

  char response[SIZEOFRESPONSE];
  for(int i=0; i<2; i++)
  {
    for(int i=0; i < SIZEOFRESPONSE; i++)
      response[i] = '\0';

    fgets(response, SIZEOFRESPONSE+1, fd);
    write_log(response);
  }
  usleep(100*200);
}


std::string Sensors::send_cmd(const char *str, DBhandler& db)
{
  std::stringstream log;
  std::string cmd_result(DEF_RES);
  std::string r_msg;
  char response[SIZEOFRESPONSE];

  /* Check the sensor type */
  std::string type = db.get_field_from_id(DB_T_SENSORS, uid.c_str(), DB_F_SENSORS_TYPE);
  std::string name = db.get_field_from_id(DB_T_SENSORS, uid.c_str(), DB_F_SENSORS_NAME); 

	if(fd)
   {
      boost::posix_time::ptime time = boost::posix_time::microsec_clock::local_time();
      boost::posix_time::time_duration duration(time.time_of_day());
      double begin = duration.total_milliseconds();
      double timeout = begin + READER_TIMEOUT_MS;
      double time_spent_ms;

      send(str, fd);

      for(int i=0; i < SIZEOFRESPONSE; i++)
        response[i] = '\0';

      fgets(response, SIZEOFRESPONSE+1, fd);
      write_log(response);
      time = boost::posix_time::microsec_clock::local_time();
      duration =  time.time_of_day();
      time_spent_ms = duration.total_milliseconds() - begin;
      write_log("This is a `various' sensor type", WARN_TYPE);

      if(strstr(response,"Wrong Command"))
      {
        fgets(response, SIZEOFRESPONSE+1, fd);
        cmd_result = "Wrong Command";
      }
      else if(type.compare("various") == 0)
      {
        cmd_result = "DONE";
/* get smth like
 * Humidity (%): 23.00
 * Temperature (oC): 22.00
 * Dew Point (oC)(`: -0.07
 * Dew PointFast (oC): -0.09
 */
        for(int i=0; i<4; i++)
        {
           for(int j=0; j < SIZEOFRESPONSE; j++)
             response[j] = '\0';

            fgets(response, SIZEOFRESPONSE+1, fd);

            /* find value position */
            char * ptr = strstr(response, "): ");
            ptr += 3 * sizeof(char);

            switch(i)
            {
              case 0:
                  if(name.compare("temperature") == 0)
                  { 
                    db.update_sensor_value(uid.c_str(), ptr); ///@TODO check ptr will hold only 1 char…)
                    log << "Sensor id:`" << uid << "' value updated: `" << ptr << "'";
                    db.log(log.str(), WARN_TYPE, time_spent_ms);

                  }
                  break;
              
              case 1:
                  if(name.compare("humidity") == 0)
                  { 
                    db.update_sensor_value(uid.c_str(), ptr); ///@TODO check ptr will hold only 1 char…)
                    log << "Sensor id:`" << uid << "' value updated: `" << ptr << "'";
                    db.log(log.str(), WARN_TYPE, time_spent_ms);

                  }
                  break;

              default:
                  write_log("don't know this line");
                  break;
            }

        }

      } // if various 
      else
      {
          db.update_sensor_value(uid.c_str(), response);
          cmd_result = "DONE";
      } 


   }
  else
   {
      write_log("write error, fd is closed", ERR_TYPE);
      cmd_result = ERR_DEVICE_CLOSED;
   }


  return cmd_result;
}

int Sensors::response_pos(const char* comment)
{
  int i,j;
  int len;
  
  len = strlen(comment);
  i = j = 0;
  while((*(comment + i + j) != '\0') && ((i + j) <= len))
   {
    if(*(comment + i + j) == ':')
     {
      j += i + 1;
      i = 0;
      continue;
     }
    i++;
   }

  return j;
}

void Sensors::send(const char* cmd, FILE* f)
{
  if(fd == NULL)
    write_log("NO FD!!!!", ERR_TYPE);

  std::stringstream log;
//  fprintf(f, "%s\r", cmd); 
  fprintf(fd, "%s\r", cmd);


  log << "command sent: `" << escape_char(cmd) << "+\\r'";
  write_log(log.str(), WARN_TYPE);
}


std::string Sensors::escape_char(const char* msg)
{
    std::string s(msg);
    size_t i = 0;

    while (true) {
      i = s.find("\n", i);
      if (i >= std::string::npos)
        break;
      s.replace(i, 2, "\\n");
      ++i;
    }

    i = 0;
    while (true) {
      i = s.find("\r", i);
      if (i >= std::string::npos)
        break;
      s.replace(i, 1, "\\r");
      ++i;
    }
    return s;
}



/* vim: set ts=2 sw=2 sts=2 et:*/
