/**  DeviceManager classe implementation
 *
 */

#include "DeviceManager.h"

using namespace std;
/** The general device manager
 */
DeviceManager::DeviceManager()
{

  db = &DBhandler::getInstance();

    if(DEBUG > 1)
        write_log("New DeviceManager");
}

/** Destructor
 */
DeviceManager::~DeviceManager()
{
    if(DEBUG > 1)
        write_log("DeviceManager destructor called");
}


/** Parse the command to return the cmd type
 * @params the reference to cmd on the following format
 * <cmd_type>#<device_type>#<device_id>#<cmd>
 * @return the cmd_type, "NULL" if not found
 * @return the passed cmd string shortened
 */
const char DeviceManager::get_cmd_type(char*& cmd)
{
//cout << cmd << endl;
  int i = 0;
  char cmd_type = *(cmd);
  cmd++;  // miss the cmd_type

  if(*(cmd) != ARG_DELIM) // New should be the delim, if not the cmd is wrong
    return  NOT_FOUND;
 
  cmd++;  // avoid the # delim
  return cmd_type;
}


/** Parse the command to return the device type
 * @params the reference to cmd on the following format
 * <device_type>#<device_id>#<cmd>
 * or
 * <device_id>#<cmd>
 * …
 * @return the fist arg, "NULL" if no `#' present
 * @return the passed cmd string shortened
 */
const char* DeviceManager::get_next(char*& cmd)
{

  char* ptr;
  int i = 0;
  string cmd_type("");
    
  ptr = strstr(cmd, ARG_DELIM_S);
  if(ptr == NULL) 
    return cmd;

  *ptr = '\0';

  while(*(cmd+i) != '\0')
    i++;

  while(*(cmd) != '\0')
  {
    cmd_type += *(cmd++);
    i++;
  }
  cmd++;

  return cmd_type.c_str();
}

/** This process the ADMIN type commands.
 * look for the right device and send the command.
 * return the response
 */
string DeviceManager::adm_cmd(const char* type, string id, const char* cmd)
{
  string answer(DEVICE_NOT_FOUND);

//cout << "type:" << type << ":id:" << id << ":cmd:" << cmd << endl;

  char* command;
  command = (char*)malloc((strlen(cmd)+1) * sizeof(char));
  strcpy(command, cmd); // needed for ion library. 

  if(strcmp(type, DB_T_READERS) == 0)
   {
      /* Checking for Redbee devices */
      Redbee_s* r = devices.redbee_list.next;
      while(r != NULL)
       {
          if(r->redbee->is_running())
           {
              if(id == r->redbee->get_uid())
               {
                  if(DEBUG > 4) write_log("I found the reader!");
                  return r->redbee->write(cmd, *db);
               }
           }
          r = r->next;
       } // while r

      /* Checking for ION devices */
      Ion_s* ions = devices.ion_list.next;
      while(ions != NULL)
       {
          if(id == ions->ion->get_uid())
           {
              if(DEBUG > 4) write_log("I found the ion reader!");
              string cc = get_next(command);
              if(strcmp(cc.c_str(), GET_PROTOCOL) == 0)
               {
                  uint32_t proto;
                  if(ions->ion->get_protocol(&proto))
                   {
                      stringstream ass;
                      ass << proto;
                      return ass.str();
                   }
                  else
                   {
                      return FAILED;
                   }
               }
              else if(strcmp(cc.c_str(), GET_EMMIT_PWR) == 0)
               {
                  uint32_t pwr;
                  if(ions->ion->get_power(&pwr))
                   {
                      stringstream ass;
                      ass << pwr;
                      return ass.str();
                   }
                  else
                   {
                      return FAILED;
                   }
               }
              else if(strcmp(cc.c_str(), GET_Q_VALUE) == 0)
               {
                  uint16_t q;
                  if(ions->ion->get_g2_q(&q))
                   {
                      stringstream ass;
                      ass << q;
                      return ass.str();
                   }
                  else
                   {
                      return FAILED;
                   }
               }
              else if(strcmp(cc.c_str(), SET_PROTOCOL) == 0)
               {
                  return "THE API FAILS?";
                  if(ions->ion->set_protocol(atoi(command)))
                    return DONE;
                  else
                    return FAILED;
               }
              else if(strcmp(cc.c_str(), SET_EMMIT_PWR) == 0)
               {
                  if(ions->ion->set_power(atoi(command)))
                    return DONE;
                  else
                    return FAILED;
               }
              else if(strcmp(cc.c_str(), SET_Q_VALUE) == 0)
               {
                  return "THE API FAILS?";
                  if(ions->ion->set_g2_q(atoi(command)))
                    return DONE;
                  else
                    return FAILED;
               }
              else
               {
                  return "NOT IMPLEMENTED";
               }
           }
          ions = ions->next;
       } // while ions 


      /* You should implement here other type of readers */

   }
  else if(strcmp(type, DB_T_SENSORS) == 0)
   {
      Sensors_s* s = devices.sensors_list.next;
      while(s != NULL)
       {   
          if(id == s->sensor->get_uid())
           {
              if(DEBUG > 4) write_log("I found the sensor!");
              return s->sensor->send_cmd(cmd, *db);
           }
          s = s->next;
       } // while s

   }
  else if(strcmp(type, DB_T_ACTUATORS) == 0)
   {
      Actuators_s* a = devices.actuators_list.next;
      while(a != NULL)
       {   
          if(id == a->actuator->get_uid())
           {
              if(DEBUG > 4) write_log("I found the actuator!");
              string nst(a->actuator->send_cmd(cmd, *db));
              return nst;
           }
          a = a->next;
       } // while s

   }
  /* same for coordinators? */

  return answer;
}

/** This send the command to the right device
 * @return the answer received
 */
string DeviceManager::process_cmd(const char* cmd)
{
  string answer;
  stringstream log;
  int id;

  char* cmd_parsed = new char[strlen(cmd)];
  strcpy(cmd_parsed, cmd);

  /* Check the command type */
  answer = CMD_UNKNOW;
  char cmd_type;
  cmd_type = get_cmd_type(cmd_parsed);
  if(cmd_type == ADM_CMD)
   {
      if(DEBUG > 4) write_log("ADMIN cmd, got a 'G'");
      
      string device_type = get_next(cmd_parsed);
      if(strcmp(device_type.c_str(), cmd_parsed) == 0)
       {
          log << "device_type not found:" << cmd;
          return log.str();
       }
      string device_id = get_next(cmd_parsed);
      if(strcmp(device_type.c_str(), cmd_parsed) == 0)
       {
          log << "device_id not found:" << cmd;
          return log.str();
       }


      answer = adm_cmd(device_type.c_str(), device_id, cmd_parsed);
   } // cmd_type == ADM_CMD
  else if (cmd_type == ACTION_CMD)
   {
      if(DEBUG > 4) write_log("ACTION_CMD cmd");
      /* Check the device type */
      answer = do_action(cmd_parsed);
    
   } // cmd_type == ACTION_CMD 
  else if (cmd_type == CTRL_CMD)
   {
      if(DEBUG > 4) write_log("CTRL_CMD cmd");
      /* Check the device type */
      answer = "DEVICE UNKNOWN";
      write_log("To implement doing controller action");

   } // cmd_type == CTRL_CMD 

//  free(cmd_parsed);  ///@TODO memory leak here. Should run valgrind!!!
  log << answer << ":" << cmd;
  return log.str();
}



/** This run across all devices queue in order to check if they have read tags
 * and run the scenario_tag func
 */
bool DeviceManager::read_tags()
{
  bool b = false;
  /* Checking for Redbee devices */
  Redbee_s* r = devices.redbee_list.next;
  while(r != NULL)
   {
      if(r->redbee->is_running())
      {
        string tag(r->redbee->get_tag_fifo());
        if(tag.compare("NULL"))
        {
          b = true;
          /* process scenario tag */
          scenario_tag(tag.c_str(), r->redbee->get_uid());
        }
      }
      r = r->next;
   }

  /* Checking for ID-20 devices */
  ID_s* ii = devices.id_list.next;
  while(ii != NULL)
   {
      if(ii->id->is_running())
      {
        string taga = ii->id->get_tag_fifo();
        if(taga.compare("NULL"))
        {
          b = true;
          /* process scenario tag */
          scenario_tag(taga.c_str(), ii->id->get_uid());
        }
      }
      ii = ii->next;
   }


  return b;
}

/** Add new device connection
 * @param eNetworkType
 * @param const char* node which could either be "/dev/ttyUSB0" or an IP adress
 * @param DeviceType (optional, default value "Endpoint" to define an endpoint
 * If the device is known, instanciate it.
 * To be recognized, the device in the DB should have either its serial written, or its VendorID and ProductID.
 */
bool DeviceManager::newDevice(eNetworkType nt, string node, string idVendor, string idProduct,  string serial, string manufacturer, string product, const char* dt)
{
  if(!update_devices(DB_T_READERS, node.c_str(), serial.c_str(), idProduct.c_str(), idVendor.c_str(), product.c_str()))
    if(!update_devices(DB_T_SENSORS, node.c_str(), serial.c_str(), idProduct.c_str(), idVendor.c_str(), product.c_str()))
      if(!update_devices(DB_T_ACTUATORS, node.c_str(), serial.c_str(), idProduct.c_str(), idVendor.c_str(), product.c_str()))
        return false;

  return true;
}

/** Check if the device is known, fill the appropriated field in the DB if so,
 * and instanciate it
 * Should merge this in newDevice but need to use same DB field for all devices type
 */
bool DeviceManager::update_devices(const char* type, const char* node, const char* serial, const char* idP, const char* idV, const char* product) 
{
  mysqlpp::StoreQueryResult res;
  int id_f, iface_f, node_f, addr_f, product_f, type_f, state_f, idproduct_f, idvendor_f, serial_f; //will hold the field number

  string iface_tf, node_tf, prod_tf, id_P_tf, id_V_tf, serial_tf;

  if(strcmp(type, DB_T_READERS) == 0)
   {
      iface_tf     = DB_F_READER_IFACE; 
      node_tf      = DB_F_READER_NODE;
      prod_tf      = DB_F_READER_PRODUCT;
      id_P_tf      = DB_F_READER_PRODUCT_ID;
      id_V_tf      = DB_F_READER_VENDOR;
      serial_tf    = DB_F_READER_SERIAL;
   }
  else if(strcmp(type, DB_T_SENSORS) == 0)
   {
      iface_tf     = DB_F_SENSORS_IFACE;
      node_tf      = DB_F_SENSORS_NODE;
      prod_tf      = DB_F_SENSORS_PRODUCT;
      id_P_tf      = DB_F_SENSORS_PRODUCT_ID;
      id_V_tf      = DB_F_SENSORS_VENDOR_ID;
      serial_tf    = DB_F_SENSORS_SERIAL;
   }
  else if(strcmp(type, DB_T_ACTUATORS) == 0)
   {
      iface_tf     = DB_F_ACTUATORS_IFACE;
      node_tf      = DB_F_ACTUATORS_NODE;
      prod_tf      = DB_F_ACTUATORS_PRODUCT;
      id_P_tf      = DB_F_ACTUATORS_PRODUCT_ID;
      id_V_tf      = DB_F_ACTUATORS_VENDOR;
      serial_tf    = DB_F_ACTUATORS_SERIAL;
   }
  else
    return false;

  res = db->get_known_devices(type);
  if(res.num_rows() < 1)
    return false;

  if(!set_field_number(res, id_f, iface_f, node_f, addr_f, product_f, type_f, state_f, idproduct_f, idvendor_f, serial_f))
    return false;

  for (size_t i = 0; i < res.num_rows(); ++i) 
   {
      if((serial_tf.compare("NULL") != 0) && (strcmp(res[i][serial_f], serial) == 0))
       {
          write_log("This new device is known");
          db->set_field(type, node_tf.c_str(), res[i][node_f], res[i][id_f]); 
          if(strlen(res[i][product_f]) < 2)
            db->set_field(type, prod_tf.c_str(), product, res[i][product_f]); 
          if(strlen(res[i][idvendor_f]) < 4)
            db->set_field(type, id_V_tf.c_str(), idV, res[i][idvendor_f]); 
          if(strlen(res[i][idproduct_f]) < 4)
            db->set_field(type, id_P_tf.c_str(), idP, res[i][idproduct_f]); 

          add_device(type, "USB", node, "", res[i][id_f], product, idP, idV, serial);
          return true;
       }
      else if((strcmp(res[i][idvendor_f], idV) == 0) &&
              (strcmp(res[i][idproduct_f], idP) == 0))
       {
          write_log("This new device is known");
          db->set_field(type, node_tf.c_str(), res[i][node_f], res[i][id_f]); 
          if(strlen(res[i][product_f]) < 2)
            db->set_field(type, prod_tf.c_str(), product, res[i][product_f]); 
          if(strlen(res[i][serial_f]) < 1)
            db->set_field(type, serial_tf.c_str(), serial, res[i][serial_f]); 

          add_device(type, "USB", node, "", res[i][id_f], product, idP, idV, serial);
          return true;
       }
   } // end for
  return false;
}

/** Remove the device (set Error=1 in the corresponding table */
void DeviceManager::removeDevice(eNetworkType nt, string node, const char* dt)
{
  stringstream log;
  log << "Removing " << node << "nt: " << nt << "dt: " << dt;
  write_log(log.str());
  write_log("To implement!!!", ERR_TYPE);

}

void DeviceManager::add_device(const char* type, const char* iface, const char* n, const char* addr, const char* id, const char* product)
{
  add_device(type, iface, n, addr, id, product, NULL, NULL, NULL);
}

/** This effectivly add a new device on the Sevices_s struct, taking the right
 * linked list from the device type and co
 */
void DeviceManager::add_device(const char* type, const char* iface, const char* n, const char* addr, const char* id, const char* product, const char* product_id, const char* vendor_id,  const char*serial)
{
  stringstream log;
  string node(n);
  if(DEBUG > 7)
    log << "Found new device, checking if known (type: `" << type << "' iface: `" << iface << "' node: `" << node << "' addr: `" << addr << "' id: `" << id << "'";


  /* preliminary checks */
  if((strcmp(iface, DB_F_READER_IFACE_USB) == 0) && (node.length() < 5))
   {   
      log << "Please check your node setting, does not appear to be correct.";
      db->log(log.str(), WARN_TYPE);
      return;
   }

  if(DEBUG > 7)
  {
    write_log(log.str(), INFO_TYPE);
    log.str("");
  }

  if(strcmp(type, DB_T_READERS) == 0)
   {
      /* Adding a new Reader */
      if(strcmp(iface, DB_F_READER_IFACE_USB) == 0)
       {
          /* If USB device, check if the right one is found */
          if(!check_usb_device(n, product_id, vendor_id, serial))
           {
              log << "Wrong node defined for Reade – Node: `" << node << "' uid: `" << id << "'";
           }
          else if(strcmp(product, DB_F_READER_PRODUCT_REDBEE) == 0)
           { 
  
              if(devices.redbee_list.add_new(node, id))
                log << "Starting new Redbee. Node: `" << node << "' uid: `" << id << "'";
              else
                log << "Redbee Node: `" << node << "' uid: `" << id << "' not found";
           }
          else if(strcmp(product, DB_F_READER_PRODUCT_INNOVATIONS) == 0)
           {
              if(devices.id_list.add_new(node, id))
                log << "Starting new ID-20. Node: `" << node << "' uid: `" << id << "'";
              else
                log << "ID-20. Node: `" << node << "' uid: `" << id << "' not found";
           }
          else
              log << "Product not implemented";

          db->log(log.str(), WARN_TYPE);
       }
      else if(strcmp(product, DB_F_READER_PRODUCT_CAENRFID_ION) == 0)
       {
// should check the ip connection. Actually try to init the lib
          if(devices.ion_list.add_new(addr, id))
            log << "Starting new Caenrfid ION. Address: `" << addr << "' uid: `" << id << "'";
          else
            log << "Could not start a new Caenrfid ION. Address: `" << addr << "' uid: `" << id << "'";
          db->log(log.str(), WARN_TYPE);
       }
        /* adding XBEE devices ?*/
        
   }
  else if(strcmp(type, DB_T_SENSORS) == 0)
   { 
      /* Adding a new Sensor */
      if(strcmp(iface, DB_F_SENSORS_IFACE_USB) == 1)
       { 
          if(devices.sensors_list.add_new(node, id))
           {
              log << "New sensor instancied. Node: `" << node << "' uid: `" << id << "'"; 
              db->log(log.str(), WARN_TYPE);
           }
          else
           {
              log << "Sensor node: `" << node << "' uid: `" << id << "' is not connected"; 
              db->log(log.str(), ERR_TYPE);
           }
       }
   }
  else if(strcmp(type, DB_T_ACTUATORS) == 0)
   {   
      /* Adding a new Actuator */
      if(strcmp(iface, DB_F_ACTUATORS_IFACE_USB) == 0)
       {
          if(devices.actuators_list.add_new(node, id))
           {
              log << "New Actuator instancied. Node: `" << node << "' uid: `" << id << "'";
              db->update_actuator(id, "OFF"); // should check if oscar type (defaut OFF while opening the connection
              db->log(log.str(), WARN_TYPE);
           }
          else
           {
              log << "Actuator node: `" << node << "' uid: `" << id << "' is not connected"; 
              db->log(log.str(), ERR_TYPE);
           }
       }
   }
/*** @TODO add coordinators */

}

/** This method get the corresponding table
 *  should parse the result, each row, each column, in order to find attached devices
 *  and define them. 
 *  @TODO  Udpate ERR = 0 if found
 */
bool DeviceManager::init_device(const char* device_type)
{
  stringstream log;

  mysqlpp::StoreQueryResult table;
  mysqlpp::StoreQueryResult res;


  /* Fills the <device_type> linked list */
  res = db->get_known_devices(device_type);
  if(res.num_rows() < 1)
    return false;

  int id_f, iface_f, node_f, addr_f, product_f, type_f, state_f, idproduct_f, idvendor_f, serial_f; //will hold the field number
  if(!set_field_number(res, id_f, iface_f, node_f, addr_f, product_f, type_f, state_f, idproduct_f, idvendor_f, serial_f))
    write_log("One field is missing! Help!", ERR_TYPE);

      for (size_t i = 0; i < res.num_rows(); ++i) 
       {
          add_device( device_type, 
                      res[i][iface_f],  res[i][node_f], 
                      res[i][addr_f],   res[i][id_f], res[i][product_f]);
       }
}

/** This method call init_device in order to fill the devices lists
 */
void DeviceManager::init()
{


  devices.redbee_list.next    = NULL;
  devices.id_list.next        = NULL;
  devices.sensors_list.next   = NULL;
  devices.actuators_list.next = NULL;
  devices.ion_list.next       = NULL;

/* Create Readers linked list */
  init_device(DB_T_READERS);

/* Create Sensors linked list */
  init_device(DB_T_SENSORS);

/* Create Actuators linked list */
  init_device(DB_T_ACTUATORS);

/* Create Coordinators linked list*/

  write_log("… Init DONE");
}

/** run a specify action from the command table
 * @arg the action id
 */
const char* DeviceManager::do_action(const char* action_id)
{
  mysqlpp::StoreQueryResult res;
  res = db->get_action(action_id); 

  if(res.num_rows() < 1)
    return "ACTION DOES NOT EXISTS";

  if( ((strcmp(res[0][DB_F_ACTION_DEVICE_TYPE], DB_T_READERS) == 0)
        || (strcmp(res[0][DB_F_ACTION_DEVICE_TYPE], "Reader") == 0) // cause the UI is broken
      ) && (strcmp(res[0][DB_F_ACTION_CMD], "inventory") == 0))
   {
      string id(res[0][DB_F_ACTION_DEVICE_ID]);

      /* Checking for ION devices */
      Ion_s* ptr = devices.ion_list.next;
      while(ptr != NULL)
       { 
          if(id == ptr->ion->get_uid())
           {
              if(DEBUG > 4) write_log("I found the ion reader!");
              return ptr->ion->do_inventory(*db);
           }
          ptr = ptr->next;
       } // while ptr
 
   }
  else if( ((strcmp(res[0][DB_F_ACTION_DEVICE_TYPE], DB_T_READERS) == 0)
             || (strcmp(res[0][DB_F_ACTION_DEVICE_TYPE], "Reader") == 0) // cause the UI is broken
           ) && (strcmp(res[0][DB_F_ACTION_CMD], "initialization") == 0))
   {
      string id(res[0][DB_F_ACTION_DEVICE_ID]);

      /* Checking for ION devices */
      Ion_s* ptr = devices.ion_list.next;
      while(ptr != NULL)
       {
          if(id == ptr->ion->get_uid())
           {
              if(DEBUG > 4) write_log("I found the ion reader!");
              ptr->ion->init();
              return "INIT DONE";
           }
          ptr = ptr->next;
       } // while ptr

   }
  else
    write_log("DUNNO this command", WARN_TYPE);

  return "DEVICE UNKNOWN";
}

/** This run all scenario triggered by a tag ID
 * @params the tag id
 * @params the reader_id who read it
 * …*/
void DeviceManager::scenario_tag(const char* tag_id, const char* reader_id)
{
    stringstream log;

  mysqlpp::StoreQueryResult scenario;
  mysqlpp::StoreQueryResult action;

  /* First log the tag*/
  db->logTag(reader_id, tag_id, "unknown", "");



  /* Check if this tag has a specific scenario (could have more than 1 */
  scenario = db->get_scenario_tag(tag_id);
  for (size_t i = 0; i < scenario.num_rows(); ++i)
   { 
      std::string reader(scenario[i][scenario.field_num(DB_F_SC_T_UID_READER)]); 
      if(reader.compare("0") != 0)   // De we defined a reader?
        if(reader.compare(reader_id) != 0)
          continue;
      std::string sensor(scenario[i][scenario.field_num(DB_F_SC_T_UID_SENSOR)]);
      if(sensor.compare("0") != 0)
        std::string sensor_val(scenario[i][scenario.field_num(DB_F_SC_T_SENSOR_VALUE)]);
      std::string location(scenario[i][scenario.field_num(DB_F_SC_T_LOC)]); 


      /* Get the action accorded to the scenario, we ask for a specific row (id)*/
      action = db->get_action(scenario[i][scenario.field_num(DB_F_SC_T_UID_ACTION)]);

      if(action.num_rows() < 1)     // this action id does not exist
        continue;

      string device_type(action[0][action.field_num(DB_F_ACTION_DEVICE_TYPE)]);
      string device_id(action[0][action.field_num(DB_F_ACTION_DEVICE_ID)]);
      string cmd(action[0][action.field_num(DB_F_ACTION_CMD)]);

      /* Correct the database name */
      if(device_type.find("Sensor") < device_type.npos)
        device_type = DB_T_SENSORS;
      else if(device_type.find("Reader") < device_type.npos)
        device_type = DB_T_READERS;
      else if(device_type.find("Tag") < device_type.npos)
        device_type = DB_T_TAGS;
      else if(device_type.find("Actuator") < device_type.npos)
        device_type = DB_T_ACTUATORS;

      
      write_log("PROCESSING SCENARIO", ERR_TYPE);
      string result = adm_cmd(device_type.c_str(), device_id.c_str(), cmd.c_str());
      log << "Have just run a Tag scenario (id=`" << scenario[i][scenario.field_num(DB_F_SC_T_UID)] << "') which results of the action id=`" << action[0][action.field_num(DB_F_ACTION_ID)] << "'. Action result=`" << result << "'." ; 
      db->log(log.str(), WARN_TYPE);
      log.str("");
   }
 
}

void DeviceManager::scenario_tag(const char* tag_id, string rid)
{
  scenario_tag(tag_id, rid.c_str());
}


/** Parse the mysqlpp result and set the int input parametrs with the correct 
 * field num. It checks the table name and select the right field name.
 */
bool DeviceManager::set_field_number(mysqlpp::StoreQueryResult r, int& id, int& iface, int& node, int& addr, int& product, int& type, int& state)
{

  if(strcmp(r.table(), DB_T_READERS) == 0)
  {
    id        = r.field_num(DB_F_READER_ID);
    iface     = r.field_num(DB_F_READER_IFACE); 
    node      = r.field_num(DB_F_READER_NODE);
    addr      = r.field_num(DB_F_READER_ADDRESS);
    product   = r.field_num(DB_F_READER_PRODUCT);
    type      = r.field_num(DB_F_READER_TYPE);
    state     = r.field_num(DB_F_READER_STATE);
  }
  else if(strcmp(r.table(), DB_T_SENSORS) == 0)
  {
    id        = r.field_num(DB_F_SENSORS_ID);
    iface     = r.field_num(DB_F_SENSORS_IFACE);
    node      = r.field_num(DB_F_SENSORS_NODE);
    addr      = r.field_num(DB_F_SENSORS_ADDRESS);
    product   = r.field_num(DB_F_SENSORS_PRODUCT);
    type      = r.field_num(DB_F_SENSORS_TYPE);
    state     = r.field_num(DB_F_SENSORS_VALUE);   // should return value as state…
    
  }
  else if(strcmp(r.table(), DB_T_ACTUATORS) == 0)
  {
    id        = r.field_num(DB_F_ACTUATORS_ID);
    iface     = r.field_num(DB_F_ACTUATORS_IFACE);
    node      = r.field_num(DB_F_ACTUATORS_NODE);
    addr      = r.field_num(DB_F_ACTUATORS_ADDRESS);
    product   = r.field_num(DB_F_ACTUATORS_PRODUCT);
    type      = r.field_num(DB_F_ACTUATORS_TYPE);
    state     = r.field_num(DB_F_ACTUATORS_STATE);
  }
  else
    return false;

/*
  else if(strcmp(device_type, DB_T_COORDINATORS) == 0)
  {
    id        = DB_F_COORDINATORS_ID;
    iface     = DB_F_COORDINATORS_IFACE;
    node      = DB_F_COORDINATORS_NODE;
    addr      = DB_F_COORDINATORS_ADDRESS;
  }
*/



}
 
bool DeviceManager::set_field_number(mysqlpp::StoreQueryResult r, int& id, int& iface, int& node, int& addr, int& product, int& type, int& state, int& idproduct, int& idvendor, int& serial)
{

  if(strcmp(r.table(), DB_T_READERS) == 0)
  {
    idproduct   = r.field_num(DB_F_READER_PRODUCT_ID);
    idvendor    = r.field_num(DB_F_READER_VENDOR);
    serial      = r.field_num(DB_F_READER_SERIAL);
  }
  else if(strcmp(r.table(), DB_T_SENSORS) == 0)
  {
    idproduct   = r.field_num(DB_F_SENSORS_PRODUCT_ID);
    idvendor    = r.field_num(DB_F_SENSORS_VENDOR_ID);
    serial      = r.field_num(DB_F_SENSORS_SERIAL);
  }
  else if(strcmp(r.table(), DB_T_ACTUATORS) == 0)
  {
    idproduct   = r.field_num(DB_F_ACTUATORS_PRODUCT_ID);
    idvendor    = r.field_num(DB_F_ACTUATORS_VENDOR);
    serial      = r.field_num(DB_F_ACTUATORS_SERIAL);
  }
  else
    return false;
/*
  else if(strcmp(device_type, DB_T_COORDINATORS) == 0)
  {
    id        = DB_F_COORDINATORS_ID;
    iface     = DB_F_COORDINATORS_IFACE;
    node      = DB_F_COORDINATORS_NODE;
    addr      = DB_F_COORDINATORS_ADDRESS;
  }
*/

  if(!set_field_number(r, id, iface, node, addr, product, type, state))
    return false;

  return true;

}


/** Check if the USB node is attached to the right device
 * @TODO to implement, check_usb_device
 */
bool DeviceManager::check_usb_device(const char* n, const char* product_id, const char* vendor_id, const char* serial)
{
/*
  if(!n || !product_id || !vendor_id || !serial)
    return false;
*/
  return true;
}


/** Threaded method
 *  @important this is the __main__ loop, everything is managed from here.
 *  Wanna add a new feature? Probably goes here.
 */
void DeviceManager::run()
{
  if(DEBUG > 1)
      write_log("DeviceManager running");

  SocketServer sock; 
  sock.start();             // Socket Server

  while(!sock.is_running()) /// Waiting for the socket server to run. @ATTENTION, only for test purpose, could block here! Should check if the thread is killed.
   {
      sleep(1);
      //end();
   }

  char msg[BUF_SIZE];
  string answer;

  stringstream log;
  Sockmsg ui_msg;

  /* Create instance of all known devices */
  init();


  while(1)
   {  
      /* Checks if command receive from socket */
      ui_msg = sock.get_oldest_msg();
      if(ui_msg.cmd.compare("")) 
       {
          if(DEBUG > 3)
           {
              log << " cmd FIFO = " << ui_msg.cmd << " from sock: " <<  ui_msg.sock ;
              write_log(log.str());
              log.str("");
           }
          strncpy(msg, ui_msg.cmd.c_str(), BUF_SIZE); /* make sure the string finish with a \0 */

          if(strstr(msg, QUIT_MSG))  /** The special string to kill my server!! */
              break;

          /* Handle the command */
          answer = process_cmd(msg);
          /* Send back the answer */
          sock.send_answer(ui_msg.sock, answer.c_str());
       }
      else
       {
          /* No command, could work alone */
          if(read_tags()) // Check all proximity readers
            db->log("A tag has just been read, check it out!");
    
/** Should run standalone scenarios here ! The scheduling ones. 
 * They are triggered by the timer.
 *  @TODO to handle timer triggered scenario, you need to create a new table, called Scenario_time for example with a time field (in ms?). The algo would be as following:
 *  1. reset timer before the while loop
 *  2. for each lines in Scenario_time
 *  2.a if timer is greater than the one on the corresponding field, run the action id 
 *  2.b else, continue
 *  3. thats'all, as told, tooks only half an hour to implement!
 *
 *  @help see the boost library for precise time (like in Actuators.cc
 *  @help don't forget to define the action to be run when activating this scenario…
 *
 * */

          

       }

      usleep(1000*10);
   } // while(1)
}


/* vim: set ts=2 sw=2 sts=2 et:*/
