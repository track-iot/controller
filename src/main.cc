/** This project goal is to fully manage RFID readers/tags
 *  avoiding collisions, and implementing many great features
 *
 *  \file main.cc
 */

#include "header.h"
#include "DeviceManager.h"
#include "USBdiscovery.h"

int main (int argc, char** argv)
{
  std::cout << "====  The DEBUG level is [" << DEBUG << "] ====\n";

/* Create threads */
  DeviceManager dm;
  USBdiscovery usbdiscovery(&dm);

/* run() threads */
  dm.start(); 
  usbdiscovery.start();     


  write_log("Enter to quit", WARN_TYPE);
  std::cin.get();

  /* Quit */
  usbdiscovery.end();
  dm.end();

  write_log("See ya!");
  return EXIT_SUCCESS;
}
/* vim: set ts=2 sw=2 sts=2 et:*/
