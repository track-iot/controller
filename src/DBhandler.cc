/**  DBhandler class implementation
 *   \file @FILE@
 *
 */

#include "DBhandler.h"

using namespace std;

 // Default Constructor
 DBhandler::DBhandler()
 {
    if(init() == false)
    {
      
      write_log("DBhandler init() failed!", ERR_TYPE);
      exit (1);
    }
    if(DEBUG > 3)
      write_log("DBhandler started");
 }


 DBhandler& DBhandler::getInstance()
 {
    static DBhandler instance;
    return instance;
 }

 // Virtual Destructor
 DBhandler::~DBhandler()
 {
   write_log("DBhandler killed!", WARN_TYPE);
 }

 /** log() in the system log database AND print in stdout
 * @params err: type of log (opt, default II)
 *  - EE
 *  - WW
 *  - II
 *  @params cstr the comment
 *  @params duration, how long the command took (opt, default O)
 *  @attention should use void write_log(std::string log, const char* err_type, int err)
 *   with the error code to get perror result.
 */
 void DBhandler::log(const char* cstr, const char* err, double duration)
 {
    if(!cstr)
      return;

    stringstream sqry;

/* console */
    sqry << cstr;
    if(duration)
      sqry << " in " << duration << "ms";
    write_log(sqry.str(), err);

    sqry.str("");

    std::string str(cstr);  // cstr is const
/* escape special char */  
    size_t i = 0;
    while (true) {
      i = str.find("'", i);
      if (i >= string::npos) 
        break;
      str.replace(i, 1, " ");
      ++i;
    }

/* dababase */
    sqry << "INSERT INTO " << DB_T_LOG_SYS;
    sqry << " (" << DB_F_LOG_SYS_TYPE     << ", ";
    sqry <<         DB_F_LOG_SYS_MSG      << ", ";
    sqry <<         DB_F_LOG_SYS_DURATION << ") ";
    sqry << " VALUE('";
    sqry <<     err       << "', '";
    sqry <<     str       << "', '";
    sqry <<     duration  << "')";

    if(DEBUG_DB_QUERY)
      write_log(sqry.str());

    mysqlpp::Query query = conn.query(sqry.str());
          mysqlpp::SimpleResult res = query.execute();
          if (!res) {
            sqry.str("");
            sqry << "Failed to get item list: " << query.error();
            write_log(sqry.str(), ERR_TYPE);
        }  
 }


/** Handling std string */
 void DBhandler::log(std::string str, const char* err, double duration)
 {
   log(str.c_str(), err, duration);
 }


 /** logNotif in the system logNotif database
 * @params cstr the string to log.
 */
 void DBhandler::logNotif(const char* cstr)
 {
    if(!cstr)
      return;

    std::string str(cstr);  // cstr is const
/* escape special char */  
    size_t i = 0;
    while (true) {
      i = str.find("'", i);
      if (i >= string::npos) 
        break;
      str.replace(i, 1, " ");
      ++i;
    }

/* dababase */
    stringstream sqry;
    sqry << "INSERT INTO " << DB_T_LOG_NOTIF;
    sqry << " (" << DB_F_LOG_NOTIF_NOTIF << ") VALUE('" << str << "')";

    send_qry(sqry.str());
 }

 /** logTag() in the system logTag database 
 * checks if the tag is known, if not known, add it in the tag table*/
 void DBhandler::logTag(const char* id_reader, const char* id_tag, const char* loc, const char* str)
 {

    if(!id_reader || !id_tag || !loc || !str)
      return;

/* dababase */
    stringstream sqry;
    sqry << "INSERT INTO " << DB_T_LOG_TAG;
    sqry << " (" << DB_F_LOG_TAG_READER_ID << ", ";
    sqry <<         DB_F_LOG_TAG_ID        << ", ";
    sqry <<         DB_F_LOG_TAG_LOC       << ", ";
    sqry <<         DB_F_LOG_TAG_ACTION    << ") ";
    sqry <<   "VALUES(";
    sqry <<      "'" << id_reader << "',";
    sqry <<      "'" << id_tag    << "',";
    sqry <<      "'" << loc       << "',";
    sqry <<      "'" << str       << "')";

    send_qry(sqry.str());

    if(is_tag_known(id_tag))
     {
        set_tag_reader(id_tag, id_reader);
     }
    else
     {
        add_tag(id_tag, id_reader, loc); 
     }
 }

bool DBhandler::set_reader_state(const char* id, const char* value)
{
    if(!id || !value)
      return false;

    stringstream sqry;

    sqry << "UPDATE " << DB_T_READERS;
    sqry << " SET "   << DB_F_READER_STATE << "='" << value << "'";
    sqry << " WHERE id=" << id;
  
    return send_qry(sqry.str());
}

bool DBhandler::set_field(const char* table, const char* field, const char* value, const char* id)
{
  if(!table || !field || !value)
    return false;

    stringstream sqry;
    std::string sid;

    if(strcmp(table, DB_T_SENSORS) == 0)
      sid = DB_F_SENSORS_ID;

    if(strcmp(table, DB_T_READERS) == 0)
      sid = DB_F_READER_ID;

    if(strcmp(table, DB_T_TAGS) == 0)
      sid = DB_F_TAGS_ID;

    if(strcmp(table, DB_T_ACTUATORS) == 0)
      sid = DB_F_ACTUATORS_ID;

    sqry << "UPDATE " << table;
    sqry << " SET "   << field << "='" << value << "'";
    sqry << " WHERE " << sid << "='" << id << "'";
  
    return send_qry(sqry.str());
}

bool DBhandler::set_tag_reader(const char* id_tag, const char* id_reader)
{
    if(!id_tag || !id_reader)
      return false;

    stringstream sqry;

    sqry << "UPDATE " << DB_T_TAGS;
    sqry << " SET "   << DB_F_TAGS_READER_ID << "='" << id_reader << "'";
    sqry << " WHERE " << DB_F_TAGS_ID << "='"  << id_tag << "'";
  
    return send_qry(sqry.str());
}

bool DBhandler::get_reader_state(const char* id)
{
    if(!id)
      return false;

    stringstream sqry;
//SELECT state FROM Readers WHERE id=362

    sqry << "SELECT " << DB_F_READER_STATE ;
    sqry << " FROM "  << DB_T_READERS ;
    sqry << " WHERE " << DB_F_READER_ID << "='" << id << "'";
  
    mysqlpp::StoreQueryResult res = get_res_qry(sqry.str());
    return res.num_rows();
}

bool DBhandler::add_tag(const char* id, const char* reader, const char* loc)
{
    if(!id || !reader || !loc)
      return false;

    stringstream sqry;
    sqry << "INSERT INTO " << DB_T_TAGS;
    sqry << " (" << DB_F_TAGS_ID          << ",";
    sqry <<         DB_F_TAGS_READER_ID   << ",";
    sqry <<         DB_F_TAGS_LOC         << ")";
    sqry <<   "VALUES(";
    sqry <<      "'" << id      << "',";
    sqry <<      "'" << reader  << "',";
    sqry <<      "'" << loc     << "')";

    send_qry(sqry.str());

    sqry.str("");
    sqry << "New tag added, id=`" << id << "' read by reader_id=`" << reader << "'";
    log(sqry.str(), WARN_TYPE);

  return true;
}


bool DBhandler::is_tag_known(const char* id)
{
    if(!id)
      return false;

    stringstream sqry;

    sqry << "SELECT " << DB_F_TAGS_ID;
    sqry << " FROM "   << DB_T_TAGS;
    sqry << " WHERE " << DB_F_TAGS_ID << "='" << id << "'";

    mysqlpp::StoreQueryResult res = get_res_qry(sqry.str());
    
  return (int)res.num_rows();
}

 bool DBhandler::init()
 {
    if(DEBUG_DB_QUERY)
      write_log("DBhandler init()");
    return (conn.connect(DB_NAME, DB_HOST, DB_LOGIN, DB_PASSW, DB_PORT));
 }

/** Function to send a mysql query
 * which does not expect result
 * @return true if success
 */
 bool DBhandler::send_qry(std::string  qry)
 {

/* console */
    if(DEBUG_DB_QUERY)
      write_log(qry);

    mysqlpp::Query query = conn.query(qry);
    mysqlpp::SimpleResult res = query.execute();
      if (!res)
       {
         stringstream err;
         err << "Failed to get item list: " << query.error();
         write_log(err.str(), ERR_TYPE);
         return false;
       }


/** display DB warning 
    if(DEBUG){
      query = conn.query("SHOW WARNINGS\G");
      res = query.execute();
      write_log(res);
    }

*/
  return true;
 }

/** Function to send a mysql query
 * @params const char* mySQL query
 * @return mysqlpp::StoreQueryResult, could be < 0 if query failed
 */
mysqlpp::StoreQueryResult DBhandler::get_res_qry(std::string qry)
{
  if(DEBUG_DB_QUERY)
    write_log(qry);

  mysqlpp::Query query = conn.query(qry);
  mysqlpp::StoreQueryResult res = query.store();
    if (!res)
     {
       stringstream err;
       err << "Failed to get item list: " << query.error();
       write_log(err.str(), ERR_TYPE);
     }

  return res;
}

mysqlpp::StoreQueryResult DBhandler::get_known_devices(const char* table)
{
  stringstream sqry;

  sqry << "SELECT *";
  sqry << " FROM "   << table;
//  sqry << " WHERE " << DB_F_TAGS_ID << "='" << id << "'";

  return get_res_qry(sqry.str());
}


mysqlpp::StoreQueryResult DBhandler::get_table_description(const char* table)
{
  stringstream sqry;

  sqry << "DESCRIBE " << table ;
  return get_res_qry(sqry.str());
}

const char* DBhandler::get_field_from_id(const char* table, const char* id, const char* field)
{
// should check result if interface does not exits in this table!!
//
  if(!table || !id || field)
    return "MISSING ARGS";


  mysqlpp::StoreQueryResult res;
  stringstream sqry;
  std::string sid;

  if(strcmp(table, DB_T_SENSORS) == 0)
    sid = DB_F_SENSORS_ID;

  if(strcmp(table, DB_T_READERS) == 0)
    sid = DB_F_READER_ID;

  if(strcmp(table, DB_T_TAGS) == 0)
    sid = DB_F_TAGS_ID;

  if(strcmp(table, DB_T_ACTUATORS) == 0)
    sid = DB_F_ACTUATORS_ID;

  sqry << "SELECT " << field << " FROM " << table << " WHERE " << sid << "='" << id << "'";
  res = get_res_qry(sqry.str()); 

  std::string s(res[0][0]); 
  return s.c_str();
}

mysqlpp::StoreQueryResult DBhandler::get_action(const char* id)
{
  stringstream sqry;

  sqry << "SELECT *";
  sqry << " FROM "   << DB_T_ACTION;
  sqry << " WHERE " << DB_F_ACTION_ID << "='" << id << "'";
  return get_res_qry(sqry.str());
}

const char* DBhandler::get_field_from_id(const char* table, int id, const char* field)
{
  if(!table || !id || !field)
    return "MISSING ARGS";

  std::stringstream ss;
  ss << id;
  std::string uid(ss.str());
  return get_field_from_id(table, uid.c_str(), field);
}

bool DBhandler::update_sensor_value(const char* id, const char* val)
{
    if(!id || !val)
      return false;

    stringstream sqry;

    sqry << "UPDATE " << DB_T_SENSORS;
    sqry << " SET "   << DB_F_SENSORS_VALUE << "='" << val << "'";
    sqry << " WHERE " << DB_F_SENSORS_ID << "=" << id;

    return send_qry(sqry.str());
}

bool DBhandler::update_actuator(const char* id, const char* val)
{
    if(!id || !val)
      return false;

    stringstream sqry;

    sqry << "UPDATE " << DB_T_ACTUATORS;
    sqry << " SET "   << DB_F_ACTUATORS_STATE << "='" << val << "'";
    sqry << " WHERE " << DB_F_ACTUATORS_ID << "=" << id;

    return send_qry(sqry.str());
}

mysqlpp::StoreQueryResult DBhandler::get_scenario_tag(const char* tag_id)
{
  stringstream sqry;

  std::string tag_uid = get_field_from_id(DB_T_TAGS, tag_id, DB_F_TAGS_UID);

  sqry << "SELECT *";
  sqry << " FROM "   << DB_T_SC_T;
  sqry << " WHERE " << DB_F_SC_T_STATE << "='" << DB_F_SC_T_STATE_ENABLE << "'";
  sqry << "   AND " << DB_F_SC_T_UID_TAG << "='" << tag_uid << "'";

  return get_res_qry(sqry.str());
}


/* vim: set ts=2 sw=2 sts=2 et:*/
