/**  ID Interface class implementation
 *
 *
 *
 */


#include "ID.h"

using namespace::std;


bool ID_s::add_new(string node, string id)
{
  /* Check if could open the port */
  int fd = open(node.c_str(), O_RDONLY | O_NOCTTY | O_NDELAY);
  if(fd == -1) 
    return false;

///@TODO should check here if serial number, Productid and Vendorid match
// if(right_device(node.c_str(), idproduct, idvendor, serial) == 0)
//  return false

  close(fd);

  ID_s* s = new ID_s;
  if(s == NULL)
    write_log("new id NULL", ERR_TYPE);
  
/*  ID id(node);
    s->id = &id;
*/
  s->id = new ID(node, id);
  s->id->start();
  s->next = next;
  next = s;
}

void ID_s::remove_next()
{
  write_log("Deleting ID", WARN_TYPE);
  ID_s* s = next;
  if(s == NULL)
    write_log("new id NULL in rm", ERR_TYPE);
  next = s->next;
  delete s->id;
  delete s;
}

std::string ID::get_port()
{
  return port;
}

string ID::get_uid()
{
  return uid;
}

ID::ID(std::string node, string id): fd(NULL), started(false)
{
    uid = id;
    port = node;
}


ID::~ID()
{
    if(DEBUG > 1)
        write_log("ID destructor");
}


/** Message sent in the following format
 * <STX><tag_id><checksum><CR><NL><ETX>
 * in ASCII, 
 * STX = 2 (start of msg)
 * CR = 13 (carriage return)
 * NL = 10 (new line)
 * ETX = 3 (end of msg)
 * <tag_id> and <checksum> are in hex.
 * <checksum> is the XOR of the 5 <tag_id> bytes (diff = wrong tag_id).
 * To get the tag_id in decimal, need to convert the five bytes in five numbers!
 * Eg. get:24800E529F87C13103
 * id=4800E529F8 == "72 0 229 41 248", don't forget to add the space separator, to get the same id as with other readers.
 * checksum=7C
 */
bool ID::read() 
{
  char response[SIZEOFRESPONSE];
  std::stringstream log;
  char c;
  int i;

  strncpy(response, "", SIZEOFRESPONSE - 1);
  fgets(response, SIZEOFRESPONSE+1, fd);  

  c = fgetc(fd);    // emptying the ETX char from fd

  std::string tag_id = get_data(response);
  if(tag_id.compare(CHECKSUM_ERROR) == 0)
  {
    cout << "ERROR the checksum is incorrect \n";
    return false;
  }

  queue.push(tag_id.c_str()); 
  return true;
}

bool ID::is_running()
{
  return started;
}

/*** Serial device initialisation
 *   -- Different as Xbee!!
 * then need
 * stty -F /dev/ttyUSB0 1:0:800008bd:0:3:1c:7f:15:1:5:1:0:11:13:1a:0:12:f:17:16:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0 
 * which corresponds to 1 parity bit, no ack, 9600b no flow control but -ixon -ixoff
 * threaded
*/

void ID::run() {
	std::stringstream log;

  fd = fopen(port.c_str(), "r+");

	if(fd){
    log << "device " << port << " receiving\n";
    write_log(log.str());
    log.str("");
    started = true;

		while (1) {

      /* read the msg awaiting */
        read();
//        sleep(1); // just to let a bit of time to write 

      usleep(1000*200);
      yield();
		}   // while(1)
		fclose(fd);
	}else
	{
        log.str("");
        log << "Error could not open port: " << port.c_str();  /// @todo test if right
        write_log(log.str(), ERR_TYPE, errno);

	}
}

ID::ID(ID&)
{

  write_log("To implement, copy constructor, id", WARN_TYPE);
}

/** Calculate the checksum (XOR with 5 first bytes,
 * return true if match (last bytes)
 */

bool ID::verif_checksum(const char* data)
{
  char byte[3];
  unsigned long csk = 0;

  for(int i = 0; i < 10; i +=2)
  {
    *(byte) = *(data+i);
    *(byte+1) = *(data+i+1);

    csk ^= strtoul(byte,NULL,16);
  }

  *(byte) = *(data+10);
  *(byte+1) = *(data+11);
  
  if(csk == strtoul(byte,NULL,16))
    return true;
  else
    return false;
}


/** parse the data, check the checksum, 
 * find out the tag id, return id or NULL if wrong checksum
 */
const char* ID::get_data(const char* data)
{
  unsigned long checksum;
  std::stringstream tag;
  char byte[3];


  data++; // cause the start char is a <STX> char.

  tag << "Reading tag: " << data;
  write_log(tag.str());
  tag.str("");

  if(!verif_checksum(data))  
  {
    cout << "WRONG checksum\n";
    return CHECKSUM_ERROR;
  }
  tag << "Read tag: " << data;
  write_log(tag.str());
  tag.str("");

  /* Getting actual id */
  for(char i = 0; i < 10; i +=2)
  {
    *(byte) = *(data+i);
    *(byte+1*sizeof(char)) = *(data+(i+1)*sizeof(char));

    tag << strtoul(byte,NULL,16);
    if(i < 8)
      tag << " ";
  }

  string t = tag.str();

  tag.str("");
  tag << "Its ID is: " << t;
  write_log(tag.str());

 return t.c_str(); 

}


/** get_oldest_msg handle the FIFO queue messages
 *  @return std::string the oldest entry
 *  @return `""' en empty string if the queue is empty
 */
std::string ID::get_oldest_msg()
{

  if(queue.empty())
    return "";

  string msg = queue.front(); /** Get oldest entry */
  queue.pop(); /** Delete the oldest entry */
  return msg;
}


std::string ID::escape_char(const char* msg)
{
    std::string s(msg);
    size_t i = 0;

    while (true) {
      i = s.find("\n", i);
      if (i >= std::string::npos)
        break;
      s.replace(i, 2, "\\n");
      ++i;
    }

    i = 0;
    while (true) {
      i = s.find("\r", i);
      if (i >= std::string::npos)
        break;
      s.replace(i, 1, "\\r");
      ++i;
    }
    return s;
}

std::string ID::get_tag_fifo()
{
  if(queue.empty())
    return "NULL";

  return  get_oldest_msg();
}

/* vim: set ts=2 sw=2 sts=2 et:*/
