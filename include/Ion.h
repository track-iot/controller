/**  Ion Interface class definition
 *
 */

#ifndef ION_H
#define ION_H

#include "header.h"
#include "DBhandler.h"
#include "Reader.h" 
#include <time.h>
#include <boost/date_time/posix_time/posix_time_types.hpp>

#ifdef __cplusplus
extern "C" {
#endif
#include <caenrfid.h>
#ifdef __cplusplus
}
#endif

#define SIZEOFRESPONSE  64
#define NULL_PORT       "/dev/null"
#define KEY_SIZE        8
#define MAX_ANTENNA     4
#define SOURCE_0        "Source_0"
#define SOURCE_1        "Source_1"
#define SOURCE_2        "Source_2"
#define SOURCE_3        "Source_3"
#define SOURCE_DISABLED "DISABLED"
#define SIZE_OF_SOURCE 9


class Ion 
{
public:
//  Ion();
  Ion(std::string node, std::string);
  virtual ~Ion();
  Ion(Ion&);
 
  const char* do_inventory(DBhandler&);
  std::string get_oldest_msg();
  std::string get_addr();
  std::string get_uid();
  void init();

  bool get_power(uint32_t*);
  bool set_power(uint32_t);
  bool get_protocol(uint32_t*);
  bool set_protocol(uint32_t);
  bool get_g2_q(uint16_t*);
  bool set_g2_q(uint16_t);
  bool check_readpoint(char* , char* , uint16_t*);
  bool add_readpoint(char* , char* );
  bool remove_readpoint(char* , char* );
  bool is_disconnected(void);


protected:

  FILE *fd;
  struct caenrfid_handle handle;
  bool disconnected;

  std::string uid;
  std::string address;
  int active_antennae;
  char antennae[MAX_ANTENNA][SIZE_OF_SOURCE];

  std::string escape_char(const char*);
  bool do_inventory(char*);
  void gen_key(char*, int);
  static char nibble2hex(char);
  char *bin2hex(uint8_t *data, size_t len);

private:

};


/* the list holding all Ion instance */
class Ion_s
{
  public:
    Ion* ion;
    Ion_s* next;

    bool add_new(std::string, std::string);
    void remove_next();  
};


#endif 
/* vim: set ts=2 sw=2 sts=2 et:*/
