/**  Device classes Definition
 *
 *
 *
 */


#ifndef _READER_H
#define _READER_H

#include "header.h"
#include "Pthread.h"

class Reader: public Pthread {
  protected:
    bool state;


  public:
     Reader(std::string);
     Reader(){}
    //Return "state" attribute
     bool isActive();

};

#endif
/* vim: set ts=2 sw=2 sts=2 et:*/
