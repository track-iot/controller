/**  Pthread Class definition
 *
 *  the class who inherist from this should override the run() method.
 *  Simply call start() to run.
 *  To wait the thread to finish, just join it using the join() method.
 *
 *  \TODO implement mutex if sync needed.
 */

//TODO: setting priority!
// nice doc http://www.yolinux.com/TUTORIALS/LinuxTutorialPosixThreads.html

#ifndef PTHREAD_H
#define PTHREAD_H

#include <pthread.h>
#include "header.h"


#define DEFAULT_PRIORITY INT 0

class Pthread{

public:

 	// Default Constructor
   	Pthread();

   	// Destructor
   	virtual ~Pthread();
  	void start();
  	void stop();
  	void end();

  	void join(void** value_ptr = NULL );
    void detach();
    void yield();

	int cancel( pthread_t );
    static void* callbck( void* ptrThis ); // NEED to be static !
  	virtual void run()=0;


private:
  	bool isDetached;
    bool isRunning;

protected:
  	pthread_attr_t   attr;
    pthread_t thread_id;
  };
#endif  // PTHREAD_H
/* vim: set ts=2 sw=2 sts=2 et:*/
