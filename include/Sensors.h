/**  Sensors class definition
 *
 *
 *
 */

#ifndef SENSORS_H
#define SENSORS_H

#include "time.h"
#include "header.h"
#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <termios.h>
#include <fcntl.h>


#define SIZEOFRESPONSE  64
#define NULL_PORT "/dev/null"
#define EOCMD_S "\n"

#define ERR_NOT_RUNNING   "33"
#define ERR_DEVICE_CLOSED "66"
#define DONE "DONE" 
#define FAIL "FAIL"  
#define READER_TIMEOUT_MS 1000
#define DEF_RES           "FFFF"
#define ERR_UID           -1


class Sensors{
public:
  Sensors(std::string, std::string);
  virtual ~Sensors();
 
  std::string send_cmd(const char*, DBhandler&);
  std::string get_port();
  std::string get_uid();

private:
  std::string uid;
  char reponse[SIZEOFRESPONSE];
  std::string port;
  std::string escape_char(const char*);
  const char* process_response(const char*, const char*);
  int response_pos(const char*);
  void send(const char*, FILE*);
  void init();

  FILE* fd;

};


/* the list holding all Sensors instance */
class Sensors_s
{
  public:
    Sensors* sensor;
    Sensors_s* next;

    bool add_new(std::string, std::string);
    bool right_device(const char*, const char*, const char*);
    void remove_next();  
};


#endif // 
/* vim: set ts=2 sw=2 sts=2 et:*/
