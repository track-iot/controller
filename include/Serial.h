/**  Serial connection class definition
 */

#ifndef SERIAL_H
#define SERIAL_H

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>


class Serial{
public:
  Serial();
  virtual ~Serial();
 
  std::string send_cmd(const char*, DBhandler&);
  std::string get_port();

private:
  std::string port;

  std::string escape_char(const char*);
  void send(const char*);
  void init_port();

  FILE* fd;
};

#endif // def SERIAL_H
/* vim: set ts=2 sw=2 sts=2 et:*/
