/**  SocketServer class definition
 *
 * \file @FILE@
 * #include "Socket.h"
 *
 */

#ifndef SocketServer_h
#define SocketServer_h

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <exception>
#include <netdb.h> 
#include <queue>

#include "header.h"
#include "Pthread.h"

#define DEFAULT_PORT   8888
#define BUF_SIZE       512
#define MAX_CLIENTS    10
#define IP_SIZE        20  // TODO update
#define ACK_STR        "ACK: "
#define MSG_DELIM      ": "
#define NAME_DELIM     ':'
#define ARG_DELIM      '#'
#define ARG_DELIM_S    "#"
#define DISCOVERY_MSG  "discovering"
#define CLOSE_SCK_MSG  "discovering:quit"

#define ADM_CMD        'G' // God mode
#define ACTION_CMD     'A'
#define NEW_DEVICE     'N'
#define UPDATE         'U'
#define CTRL_CMD       'C'

#define GET_PROTOCOL   "GR"
#define GET_EMMIT_PWR  "GP"
#define GET_Q_VALUE    "GQ"
#define SET_PROTOCOL   "SR"
#define SET_EMMIT_PWR  "SP"
#define SET_Q_VALUE    "SQ"

typedef int SOCKET;

typedef struct
{
   SOCKET sock;
   char ip[IP_SIZE];  // IP from which computer the user is connected @TODO be dynamic
//   char* name;        // The username # not used, it's on the socket message
}Client;

typedef struct
{
  std::string cmd;
  SOCKET sock;
  std::string name;
}Sockmsg;

typedef std::queue<Sockmsg> msg_queue;


class SocketServer : public virtual Pthread{
public:
  SocketServer();
  SocketServer(int port);
  virtual ~SocketServer();

 // bool setDefaultVal(int port, int newBacklog = NULL); ///Goes to update the DBB or file, would be udptaded at next reboot. @return false if fails
  virtual void run();///The run methode implements a thread. Waiting for socket connections.
  void broadcast_message(Client*, const char*);     /// Broadcast a message to all client
  void broadcast_message(Client*, Client, const char*); /// Adding the sender, we want him to receive an ACK
  Sockmsg get_oldest_msg();
  bool is_running();
  void send_answer(SOCKET, const char*);
  
private:

  bool init();
  void remove_client(Client* , int);
  bool add_client(Client*, fd_set&, int, int&);
  void disconnect_all_clients(Client*);
  void write_to_client(SOCKET, const char*);
  int read_client(SOCKET, char*);
  void q_push(std::string, SOCKET);

  int localPort;
  bool running;
  int srvSocket;
  int active_connection_nbr; //actual
//  DBhandler* dbhandlr;
  //std::queue<sockmsg> queue;
  msg_queue queue;
};


#endif // SocketServer_h

/* vim: set ts=2 sw=2 sts=2 et:*/
