/**  Actuators class definition
 *
 *
 *
 */

#ifndef ACTUATORS_H
#define ACTUATORS_H

#include "time.h"
#include "header.h"
#include <termios.h>
#include <fcntl.h>


#define SIZEOFRESPONSE  64
#define NULL_PORT "/dev/null"
#define EOCMD_S "\n"

#define ERR_NOT_RUNNING   "33"
#define ERR_DEVICE_CLOSED "66"
#define DONE "DONE" 
#define FAIL "FAIL"  
#define READER_TIMEOUT_MS 1000
#define DEF_RES           "FFFF"
#define ERR_UID           -1


class Actuators{
public:
  Actuators(std::string, std::string);
  virtual ~Actuators();
 
  std::string send_cmd(const char*, DBhandler&);
  std::string get_port();
  std::string get_uid();

private:
  std::string uid;
  char reponse[SIZEOFRESPONSE];
  std::string port;
  std::string escape_char(const char*);
  const char* process_response(const char*, const char*);
  void send(const char*);
  void init();

  FILE* fd;
};


/* the list holding all Redbee instance */
class Actuators_s
{
  public:
    Actuators* actuator;
    Actuators_s* next;

    bool add_new(std::string, std::string);
    bool right_device(const char*, const char*, const char*);
    void remove_next();  
};


#endif // def ACTUATORS_H
/* vim: set ts=2 sw=2 sts=2 et:*/
