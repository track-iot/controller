/**  Redbee Interface class definition
 */

#ifndef REDBEE_H
#define REDBEE_H

#include "header.h"

#include <semaphore.h>
#include <queue>
#include <time.h>
#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <termios.h>
#include <fcntl.h>


#include "DBhandler.h"
#include "Reader.h" 


#define SIZEOFRESPONSE  64
#define NULL_PORT "/dev/null"
#define MAX_TRY   3
#define EOCMD "\r\n"

#define ERR_NOT_RUNNING   "33"
#define ERR_DEVICE_CLOSED "66"
#define DONE "DONE"    // should return a value
#define FAIL "FAIL"    // should return a value
#define READER_TIMEOUT_MS 1000
#define DEF_RES           "FFFF"
#define ERR_UID           -1


class Redbee : public Reader{
public:
//  Redbee();
  Redbee(std::string node, std::string);
  virtual ~Redbee();
  Redbee(Redbee&);
 
  std::string write(const char* str, DBhandler&);
  virtual void run();
  std::string get_oldest_msg();
  std::string get_tag_fifo();
  bool is_running();
  std::string get_port();
  std::string get_uid();



protected:

  FILE *fd;
  bool disconnected;
  std::string uid;
  std::queue<std::string> queue;
  sem_t sem_node;
  sem_t sem_queue;
  bool finish;
  char reponse[SIZEOFRESPONSE];
  std::string port;
  bool started;
  std::string escape_char(const char*);
  bool read(void);
  void send(const char*, DBhandler&);
  int response_pos(const char*);
  bool is_msg_tag(std::string);

  bool busy;
private:
};


/* the list holding all Redbee instance */
class Redbee_s
{
  public:
    Redbee* redbee;
    Redbee_s* next;

    bool add_new(std::string, std::string);
    bool right_device(const char*, const char*, const char*);
    void remove_next();  
};


#endif // def REDBEE_H
/* vim: set ts=2 sw=2 sts=2 et:*/
