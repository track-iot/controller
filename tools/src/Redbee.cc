/**  Redbee Interface class implementation
 *
 *
 *
 */


#include "Redbee.h"

using namespace::std;


bool Redbee_s::add_new(string node, string id)
{
///@TODO should check here if serial number, Productid and Vendorid match
// if(right_device(node.c_str(), idproduct, idvendor, serial) == 0)
//  return false

  Redbee_s* s = new Redbee_s;
  if(s == NULL)
    write_log("new redbee NULL", ERR_TYPE);
  
  s->redbee = new Redbee(node, id);
  s->redbee->start();
  if(!s->redbee->is_running())
   {
      delete(s);
      return false;
   }
  else
   {
      s->next = next;
      next = s;
   }
  return true;
}

void Redbee_s::remove_next()
{
  write_log("Deleting Redbee", WARN_TYPE);
  Redbee_s* s = next;
  if(s == NULL)
    write_log("new redbee NULL in rm", ERR_TYPE);
  next = s->next;
  delete s->redbee;
  delete s;
}

std::string Redbee::get_port()
{
  return port;
}

string Redbee::get_uid()
{
  return uid;
}

Redbee::Redbee(std::string node, string id): fd(NULL), started(false), busy(false), disconnected(false)
{
    uid = id;
    port = node;

    sem_init(&sem_node, 0, 1); /** protecting the device node */
    sem_init(&sem_queue, 0, 1); /* protecting the queue */
}


Redbee::~Redbee()
{
    if(DEBUG > 1)
        write_log("Redbee destructor");
    sem_destroy(&sem_node);
    sem_destroy(&sem_queue);
}


/** Message sent in the following format
 * \r\n@2:T:NACK 123 192 0 0 0\r\n>
 */
bool Redbee::read() 
{
  char response[SIZEOFRESPONSE];
  std::stringstream log;
  char c;
  int i;

  strncpy(response, "", SIZEOFRESPONSE - 1);

  fgets(response, SIZEOFRESPONSE+1, fd);  

  if(*(response) == '\r') // fist \r\n
    fgets(response, SIZEOFRESPONSE+1, fd);   // fist \r\n

  /* Clean the string */
  while(*(response+i*sizeof(char)) != '\0') /* There should always be an ending \0, thanks to strncpy */
  {
    if((*(response+i*sizeof(char)) == '\n') || (*(response+i*sizeof(char)) == '\r'))
      *(response+i*sizeof(char)) = '\0';
    i++;
  }
  if(DEBUG > 3) log << "got:\t`" << escape_char(response) << "'\n";

  c = fgetc(fd); // the final >
  if(DEBUG > 3) log << "and:\t`" << c << "'";

  if(DEBUG > 3) write_log(log.str());
  log.str("");

  if(*(response) == '\0')
  {
    write_log("EMPTY TAG??", ERR_TYPE);
    return false;
  }

  
  if(strlen(response) <= 1) /// TODO check if could be one char
    return false;
  
  if(DEBUG>1)
   {
      log << "Adding in queue: `" << response+response_pos(response)*sizeof(char) << "'";
      write_log(log.str());
      log.str("");
   }

  queue.push(response+response_pos(response)*sizeof(char)); /* used to remove "@2:T:" */
  return true;
}

bool Redbee::is_running()
{
  while(started && !disconnected); //We wait for the device to have started or failed

  return started;
}

/*** Serial device initialisation
 *   -- XXbee module and RedBee modules
 * then need
 * stty -F /dev/ttyUSB0 1:0:800008bd:0:3:1c:7f:15:1:5:1:0:11:13:1a:0:12:f:17:16:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0
 * which corresponds to 1 parity bit, no ack, 9600b no flow control but -ixon -ixoff
 * threaded
*/

void Redbee::run() {
	std::stringstream log;

  fd = fopen(port.c_str(), "r+");


	if(fd){
    fd_set rdfs;
    int fd_fd = fileno(fd);
    timeval timeout;
    log << "device " << port << " receiving\n";
    write_log(log.str());
    log.str("");
    started = true;

		while (1) {

      FD_ZERO(&rdfs);
      FD_SET(fd_fd, &rdfs);
      timeout.tv_sec = 0;
      timeout.tv_usec = 300;
      if(select(fd_fd + 1, &rdfs, NULL, NULL, &timeout) == -1)
       {
          perror("select()");
          exit(errno);
       }

      if(FD_ISSET(fd_fd, &rdfs) > 0)
       {
      /* read the msg awaiting */
        if(read() == false)
          write_log("Reading error in Redbee", ERR_TYPE);
//        sleep(1); // just to let a bit of time to write 
       }

      usleep(1000*200);
      yield();
		}   // while(1)
		fclose(fd);
	}else
	{
    disconnected = true;
    log.str("");
    log << "Error could not open port: " << port.c_str();  /// @todo test if right
    write_log(log.str(), ERR_TYPE, errno);
	}
}

Redbee::Redbee(Redbee&)
{

  write_log("To implement, copy constructor, redbee", WARN_TYPE);
}

std::string Redbee::write(const char *str, DBhandler& db)
{
  std::stringstream log;
  char response[SIZEOFRESPONSE];
  int loop = 0;

  strncpy(response, "", SIZEOFRESPONSE - 1); // init
  
  std::string cmd_result(DEF_RES);
 
  int looppp = 0;
  if(!started)
  {
    if(DEBUG) write_log("device NOT started", ERR_TYPE);
    return ERR_NOT_RUNNING;
  }

  while(busy);
  busy = true;

  std::string r_msg;

  sem_wait(&sem_queue);
  if(DEBUG > 3) write_log(">>> sem QUEUE locked   write", WARN_TYPE);

	if(fd)
  {
    boost::posix_time::ptime time = boost::posix_time::microsec_clock::local_time();
    boost::posix_time::time_duration duration(time.time_of_day());
    double begin = duration.total_milliseconds();
    double timeout = begin + READER_TIMEOUT_MS;
    double time_spent_ms;

    send(str, db);
    
		do 
    {

      r_msg = get_oldest_msg();

      while(is_msg_tag(r_msg) && (looppp < 5)) 
      {
          log << " got a tag, pushing in the queue: `" << r_msg << "'";
          write_log(log.str());
          log.str("");
          queue.push(r_msg.c_str());
          r_msg = get_oldest_msg();
          looppp++;
      }

      time = boost::posix_time::microsec_clock::local_time();
      duration =  time.time_of_day();
      time_spent_ms = duration.total_milliseconds() - begin;

      if(r_msg.compare("") == 0)
      {
        /* I __need__ a reply ! */

        if(time_spent_ms > 800)
        {
          log << "Timeout while getting answer. cmd: `" << str << "', waited for: " << time_spent_ms << " ms";
          db.log(log.str(), ERR_TYPE, time_spent_ms);
          log.str("");
          break;
        }

        usleep(1000*10);
        continue; 
      }

      if(loop > MAX_TRY)
       {
          write_log("MAX try", ERR_TYPE);
          cmd_result = "MAX_TRY";
       }

      strncpy(response, r_msg.c_str(), SIZEOFRESPONSE - 1); // TODO j'étais partis avec du c sting, pas de l'std::string… uniformiser

      log << "Command sent: `" << str << "', response is: " << response;
      db.log(log.str(), INFO_TYPE, time_spent_ms);
      log.str("");

			if (strstr(response, "NACK")) // should we ? Don't think so
       {

          log << "The command `" << str << "' was not ack from the device.";
          db.log(log.str(), ERR_TYPE, time_spent_ms);
          log.str("");
          cmd_result = FAIL;
          break;
			 }
      else if (strstr(response, "ACK")) // I WANT ACK in the answer, but not NACK
       {
          if(strstr(str, "rfp 1"))
           {
              db.set_reader_state(uid.c_str(), DB_F_READER_STATE_ACTIVE);
              log << "Set Reader `" << uid.c_str() << "' state `" << DB_F_READER_STATE_ACTIVE << "'";
              db.log(log.str(), INFO_TYPE, time_spent_ms);
              log.str("");
              cmd_result = DONE;

           }
          else if(strstr(str, "rfp "))
           {
              db.set_reader_state(uid.c_str(), DB_F_READER_STATE_INACTIVE);
              log << "Set Reader `" << uid.c_str() << "' state `" << DB_F_READER_STATE_INACTIVE << "'";
              db.log(log.str(), INFO_TYPE, time_spent_ms);
              log.str("");
              cmd_result = DONE;

           }
          else
             write_log("    +++ ACK but unknown command", ERR_TYPE); ///TODO implement
          cmd_result = response;
       }
       write_log("    +++ ACK but unknown result", ERR_TYPE); ///TODO implement
       cmd_result = response;

      time = boost::posix_time::microsec_clock::local_time();
      duration =  time.time_of_day();

		}while((cmd_result.compare(DEF_RES) == 0) && (duration.total_milliseconds() < timeout)); // while

	}else
   {
        write_log("write error, fd is closed", ERR_TYPE);
        cmd_result = ERR_DEVICE_CLOSED;
    }
  sem_post(&sem_queue);
  if(DEBUG > 3) write_log("<<< sem QUEUE released write", WARN_TYPE);
  busy = false;
  return cmd_result; 
}


bool Redbee::is_msg_tag(std::string str)
{
/* A msg containing the id is
 * at least "ACK 0 0 0 0 0"
 * at most  "NACK 255 255 255 255 255"
 */

  int len = str.length();
  if((len > 12) && (len < 23))  /* at least we have "ACK 0 0 0 0 0" */
    if(str.find("ACK") < 2)
      return true;

  return false;
}

void Redbee::send(const char* cmd, DBhandler& db)
{
  std::stringstream log;

///@TODO should get @0/2: from the database
//

  fprintf(fd, "%s%s%s%s%s", "@",db.get_field_from_id(DB_T_READERS, uid.c_str(), DB_F_READER_ADDRESS), ":", cmd, EOCMD);   //add the \r\n

  //fprintf(fd, "%s%s", cmd, "\r\n");   //add the \r\n //@TODO should update accordinngly to usb node

  log << "command sent: `" << cmd << "'";
  write_log(log.str(), WARN_TYPE);

}



/** tag read like 
 * `@2:T:ACK 69 0 184 206 104'
 * @return NULL if no tag
 */
std::string Redbee::get_tag_fifo()
{
  std::stringstream log;
  char* id_tag;
  char* comment;
  char response[SIZEOFRESPONSE];
  int j;

  if(busy || queue.empty())
    return "NULL";
  busy = true;

  sem_wait(&sem_queue);
  if(DEBUG > 3) write_log(">>> sem QUEUE locked   process", WARN_TYPE);
  string msg =  get_oldest_msg(); 
  sem_post(&sem_queue);
  if(DEBUG > 3) write_log("<<< sem QUEUE released process", WARN_TYPE);

  if(DEBUG > 3)
  {
    log << " reader FIFO: `" << msg << "'";
    write_log(log.str());
    log.str("");
  }
  
  /* process */


  strncpy(response, msg.c_str(), SIZEOFRESPONSE - 1);

  id_tag = strstr(response, " "); // the id start at fist blank space in the RedBee
  if(id_tag == NULL){   // is the device removed?
    write_log("id_tag not found", ERR_TYPE);
    return "NULL";
  }

//  int len = strlen(response) - strlen(id_tag);
  id_tag++; // just to remove the trailing space

/*
  comment = (char*)malloc((len + 1) * sizeof(char)); //need to add the \0
  strncpy(comment, response, len);
  *(comment + len) = '\0';


//  db.logTag(uid.c_str(), id_tag, "unknown", comment);

  free(comment);
*/
  busy = false;
std::cout << "«««« tag " << id_tag << std::endl;
  std::string tt(id_tag);
  return tt;
}


int Redbee::response_pos(const char* comment)
{
  int i,j;
  int len;
  
  len = strlen(comment);
  i = j = 0;
  while((*(comment + i + j) != '\0') && ((i + j) <= len))
   {
    if(*(comment + i + j) == ':')
     {
      j += i + 1;
      i = 0;
      continue;
     }
    i++;
   }

  return j;
}


/** get_oldest_msg handle the FIFO queue messages
 *  @return std::string the oldest entry
 *  @return `""' en empty string if the queue is empty
 */
std::string Redbee::get_oldest_msg()
{

  if(queue.empty())
    return "";

  string msg = queue.front(); /** Get oldest entry */
  queue.pop(); /** Delete the oldest entry */
  return msg;
}


std::string Redbee::escape_char(const char* msg)
{
    std::string s(msg);
    size_t i = 0;

    while (true) {
      i = s.find("\n", i);
      if (i >= std::string::npos)
        break;
      s.replace(i, 2, "\\n");
      ++i;
    }

    i = 0;
    while (true) {
      i = s.find("\r", i);
      if (i >= std::string::npos)
        break;
      s.replace(i, 1, "\\r");
      ++i;
    }
    return s;
}



/* vim: set ts=2 sw=2 sts=2 et:*/
