/**  Actuators class implementation
 *
 *
 *
 */


#include "Actuators.h"
#include <boost/date_time/posix_time/posix_time_types.hpp>

using namespace::std;


bool Actuators_s::add_new(string node, string id)
{
  /* Check if could open the port */
  int fd = open(node.c_str(), O_RDWR | O_NOCTTY | O_NDELAY);
  if(fd == -1) 
    return false;

// TODO should check here if serial number, Productid and Vendorid match
// if(right_device(node.c_str(), idproduct, idvendor, serial) == 0)
//  return false

  close(fd);

  Actuators_s* s = new Actuators_s;
  if(s == NULL)
    write_log("new actuator NULL", ERR_TYPE);
  
  s->actuator = new Actuators(node, id);
  s->next = next;
  next = s;
}

void Actuators_s::remove_next()
{
  write_log("Deleting actuator", WARN_TYPE);
  Actuators_s* s = next;
  if(s == NULL)
    write_log("new actuator NULL in rm", ERR_TYPE);
  next = s->next;
  delete s->actuator;
  delete s;
}

std::string Actuators::get_port()
{
  return port;
}

string Actuators::get_uid()
{
  return uid;
}


Actuators::Actuators(std::string node, string id): fd(NULL)
{
    uid = id;
    port = node;
    init();
}


Actuators::~Actuators()
{
    fclose(fd);
    if(DEBUG > 1)
        write_log("Actuators destructor");
}

const char* Actuators::process_response(const char* response, const char* type)
{
  return "please implement me";
}

void Actuators::init()
{
  fd = fopen(port.c_str(), "r+");
  if(fd == NULL)
    write_log("COULD NOT OPEN PORT", ERR_TYPE);
  usleep(1000*200);

  char response[SIZEOFRESPONSE];
  for(int i=0; i<2; i++)
  {
    for(int i=0; i < SIZEOFRESPONSE; i++)
      response[i] = '\0';

    fgets(response, SIZEOFRESPONSE+1, fd);
    write_log(response);
  }
}


std::string Actuators::send_cmd(const char *str, DBhandler& db)
{
  std::stringstream log;
  std::string cmd_result(DEF_RES);
  std::string r_msg;
  char response[SIZEOFRESPONSE];


  /* Check the actuator type */
  std::string type = db.get_field_from_id(DB_T_ACTUATORS, uid.c_str(), DB_F_ACTUATORS_TYPE);
  std::string name = db.get_field_from_id(DB_T_ACTUATORS, uid.c_str(), DB_F_ACTUATORS_NAME); 


	if(fd)
   {
      boost::posix_time::ptime time = boost::posix_time::microsec_clock::local_time();
      boost::posix_time::time_duration duration(time.time_of_day());
      double begin = duration.total_milliseconds();
      double timeout = begin + READER_TIMEOUT_MS;
      double time_spent_ms;


      //usleep(1000 * 100);

      send(str);

      for(int i=0; i < SIZEOFRESPONSE; i++)
        response[i] = '\0';

      fgets(response, SIZEOFRESPONSE+1, fd);
      write_log(response);

      time = boost::posix_time::microsec_clock::local_time();
      duration =  time.time_of_day();
      time_spent_ms = duration.total_milliseconds() - begin;

      if(strstr(response,"Wrong Command"))
      {
        fgets(response, SIZEOFRESPONSE+1, fd); //empty the stack (read last line)
        cmd_result = "Wrong Command";
      }
      else 
      {
        cmd_result = "DONE";
/* get smth like
 * Actuator OFF
 */

        if(strcmp(response, " ON")) // avoid the ending \n
        {

          db.update_actuator(uid.c_str(), "ON"); 
          log << "Actuator id:`" << uid << "' state updated: `" << "ON" << "'";
          db.log(log.str(), WARN_TYPE, time_spent_ms);
        }
        else if (strcmp(response, " OFF"))
        {
          
          db.update_actuator(uid.c_str(), "OFF"); 
          log << "Actuator id:`" << uid << "' state updated: `" << "OFF" << "'";
          db.log(log.str(), WARN_TYPE, time_spent_ms);
        }

      } // if various 

   }
  else
   {
      write_log("write error, fd is closed", ERR_TYPE);
      cmd_result = ERR_DEVICE_CLOSED;
   }


  return cmd_result;
}

void Actuators::send(const char* cmd)
{
  if(fd == NULL)
    write_log("NO FD!!!!", ERR_TYPE);

  std::stringstream log;
//  fprintf(f, "%s\r", cmd); 
  fprintf(fd, "%s\r", cmd);


  log << "command sent: `" << escape_char(cmd) << "+\\r'";
  write_log(log.str(), WARN_TYPE);
}


std::string Actuators::escape_char(const char* msg)
{
    std::string s(msg);
    size_t i = 0;

    while (true) {
      i = s.find("\n", i);
      if (i >= std::string::npos)
        break;
      s.replace(i, 2, "\\n");
      ++i;
    }

    i = 0;
    while (true) {
      i = s.find("\r", i);
      if (i >= std::string::npos)
        break;
      s.replace(i, 1, "\\r");
      ++i;
    }
    return s;
}



/* vim: set ts=2 sw=2 sts=2 et:*/
