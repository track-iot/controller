/**  Pthread class implementation
 *   \file @FILE@
 *
 */

#include "Pthread.h"


 // Default Constructor
 Pthread::Pthread(): isDetached(0), isRunning(0)
 {
 //  if(priority != DEFAULT_PRIORITY)

 }

 // Virtual Destructor
 Pthread::~Pthread()
 {
   stop();
   end();
  write_log("Pthread killed!");
 }

//@todo this fails, SEGFAULT here!
 void Pthread::stop() {
    if(isRunning)
    {
       if (pthread_cancel(thread_id)) //there
          write_log("Pthread cancel stop fails", ERR_TYPE, errno);
    }else
      write_log("trying to stop a not running thread", ERR_TYPE);
 }

 void Pthread::end() {
   pthread_exit(0);
 }

 void Pthread::detach()
 {
   if ( isDetached ) return;

   if(!pthread_detach( thread_id ))
     isDetached = 1;
   else
      write_log("detach() ERROR", ERR_TYPE);
 }

 // Join PThread
 void Pthread::join(void** value_ptr)
 {
   if ( !isDetached ) return;

   if(!pthread_join(thread_id, value_ptr))
     isDetached = 0;
   else
    write_log("join() ERROR", ERR_TYPE);
  }

//SOURCE:http://j2k.sourceforge.net/manual/manual2.html
void* Pthread::callbck( void* ptrThis )
{
     Pthread* me = (Pthread*)ptrThis;
     me->isRunning = true;
     me->run();      // should have an infinite loop. This is what you want to override.
     printf( "in callbck");
     pthread_exit( NULL );

     return NULL;
}

//@todo: check
 void Pthread::start()
 {
    if(DEBUG>1)
      write_log("new thread creation");
   memset( &attr,  0, sizeof( attr  ) );
   register int rc = 0;

   rc = pthread_attr_init(&attr );
   rc = pthread_attr_setinheritsched( &attr, PTHREAD_EXPLICIT_SCHED );
   rc = pthread_attr_setdetachstate( &attr, PTHREAD_CREATE_DETACHED );


   rc = pthread_create( &thread_id, &attr, &callbck, (void*) this);
   rc = pthread_attr_destroy(&attr);
   pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
 //  pthread_setcanceltype(PTHREAD_CANCEL_DEFERRED, NULL);

   if ( rc == -1 )
      write_log("Forgot to include libraries:" "g++ -lpthread -lrt -lsocket -lnsl", ERR_TYPE);

}

void Pthread::yield()
{
   pthread_yield();
}


/* vim: set ts=2 sw=2 sts=2 et:*/
