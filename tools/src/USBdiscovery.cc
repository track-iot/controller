/**  USBdiscovery class implementation
 *
 *   \file USBdiscovery.h 
 *   @attention You need to update the udev rule. See in tools folder
 */

#include "USBdiscovery.h"
using namespace std;

//class DeviceManager;

//constructor
USBdiscovery::USBdiscovery(DeviceManager* manager)
{

  init();
	mode_t mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH ;
	/* User can read, write ; groupe member can only read, like other */
  unlink(PATH_PIPE); //prevent
	/* Create the named pipe */
	if(mkfifo(PATH_PIPE, mode) < 0)
	{
    write_log("Error creating the named pipe", ERR_TYPE, errno);
	  exit(EXIT_FAILURE) ;
	}
  dm = manager;
}

//destructor, delete pipe
USBdiscovery::~USBdiscovery()
{
	if((unlink(PATH_PIPE)) < 0)
	{
    write_log("Error removing the named pipe", ERR_TYPE, errno);
	  exit(EXIT_FAILURE) ;
	}
}

void USBdiscovery::init()
{
  idVendor = "";
  idProduct = "";
  p = "";
  N = "";
  serial = "";
  manufacturer = "";
  product = "";
}


bool USBdiscovery::splitData(std::string str)
{
  int pos=0;
  const char* delim=" ";
  const char* delim2="| ";

  string field1, field2;

  pos = str.find(delim);
  field1 = str.substr(0,pos);
  str.erase(0,pos + 1);

  pos = str.find(delim);
  field2 = str.substr(0,pos);
  str.erase(0,pos + 1);

  char * c_str;
  c_str = (char*)malloc((str.length()+1) * sizeof(char));
  strncpy(c_str, str.c_str(), str.length());

  int i = 0;
  int j = 0;
  while(*(c_str+i) != '\0')
  {
    if(*(c_str+i) == ' ')// delim
      j++;
    i++;
  }

  if(idVendor.compare("") != 0)   // Second iteration
  {
    p = field1;
    N = field2;
    return true;
  }

/* Because we want to match only one specific message.
 * Ex. Only the fist one:
 * 05d5 0624 /devices/pci0000:00/0000:00:13.2/usb2/2-2/2-2.3/2-2.3.2 /dev/bus/usb/002/027  KB| Multimedia keyboard
 * /devices/pci0000:00/0000:00:13.2/usb2/2-2/2-2.3/2-2.3.2/2-2.3.2:1.0/0003:05D5:0624.002F/hidraw/hidraw4 /dev/hidraw4  | 
 * /devices/pci0000:00/0000:00:13.2/usb2/2-2/2-2.3/2-2.3.2/2-2.3.2:1.1/0003:05D5:0624.0030/hidraw/hidraw5 /dev/hidraw5  | 
 * /devices/pci0000:00/0000:00:13.2/usb2/2-2/2-2.3/2-2.3.2/2-2.3.2:1.0/input/input49/event7 /dev/input/event7  | 
 * /devices/pci0000:00/0000:00:13.2/usb2/2-2/2-2.3/2-2.3.2/2-2.3.2:1.1/input/input50/event8 /dev/input/event8  | 
 */
  if(j < 4)
    return false;
  
  idVendor = field1;
  idProduct = field2;

  pos = str.find(delim);  // don't need %p
  str.erase(0,pos + 1);

  pos = str.find(delim);  // don't need %N
  str.erase(0,pos + 1);

  pos = str.find(delim);
  serial = str.substr(0,pos);
  str.erase(0,pos + 1);
  if(serial.compare("") == 0)
    serial = "NULL";

  pos = str.find(delim2);
  manufacturer = str.substr(0,pos);
  str.erase(0,pos + 1);

  product = str; 
  product.erase(0, 1);                      // remove the first space
  product.erase(product.length()-1, 1);     // remove the trailing \n

  return false;
}

void USBdiscovery::run() { // Log only through the device manager

	int fd, len;
	char buff [BUFF_SIZE]={0};
  char* arg;
  const char* removing="removing ";     // removing special string sent

	fd = open(PATH_PIPE, O_RDONLY);
	while(1)
  {
		len = read(fd, buff, BUFF_SIZE);
    if(len < 0)
    {
      write_log("error while reading", ERR_TYPE);
      break;
    }
    else if(len == 0)   /* Nothing read */
      continue;

    arg = strstr(buff, removing);
    if(arg != NULL){   // is the device removed?
      //write_log("Device Removed", WARN_TYPE);
      arg += strlen(removing);
      dm->removeDevice(Serial, arg);
/* should do the match, cause we could get
 * removing /devices/pci0000:00/0000:00:13.2/usb2/2-2/2-2.3/2-2.3.3/2-2.3.3:1.0/ttyUSB0/tty/ttyUSB0
 * removing /devices/pci0000:00/0000:00:13.2/usb2/2-2/2-2.3/2-2.3.3/2-2.3.3:1.0/ttyUSB0
 * removing /devices/pci0000:00/0000:00:13.2/usb2/2-2/2-2.3/2-2.3.3/2-2.3.3:1.0
 * removing /devices/pci0000:00/0000:00:13.2/usb2/2-2/2-2.3/2-2.3.3
 *
 * would be slow to parse the db, creating a string NODE in the class is faster!!!!
 */
    }
    else
    {
        if(splitData(buff))
        {
          std::stringstream log;
          log << "\n −−−−−−−−−−−−−−−−−−−−−−−−−−−−−− "<< endl;
          log << " N           : " << N            << endl;
          log << " idVendor    : " << idVendor     << endl;
          log << " idProduct   : " << idProduct    << endl;
          log << " serial      : " << serial       << endl;
          log << " manufacturer: " << manufacturer << endl;
          log << " product     : " << product      << endl;
          log << " −−−−−−−−−−−−−−−−−−−−−−−−−−−−−− "<< endl;
          write_log(log.str());
        
          if(dm->newDevice(Serial, N, idVendor, idProduct, serial, manufacturer, product)) ///@todo correct newDevice from serial always an Endpoint, should check from serial if RedBee or Zigbee
//
            write_log("Device added", WARN_TYPE);
          else
            write_log("Device unknown", WARN_TYPE);
          init();
        }
    }
  //  while(srtsep

/** Receive code:
 * The protocol is as following. We receive:
 * 1. <idVendor> <idProduct> <%p> <%N> <serial> <manufacturer>| <product>
 *    Then we store all but <%N> which is not revelent at this time.
 *    <manufacturer> and <product> could have spaces, use "|".
 * 2. <%p> <%N>
 *    We check if <%p> is known and <%N> unknown, then we override <%p> with the child value and save <%N>
 * 3. removing <%p>
 *    We check if the device was connected, if yes, we delete it.
 *    We would receive many of this strings, as it is removing all parents of this device
 *    The real device node is after the last "/". See:
 *    removing /devices/pci0000:00/0000:00:1a.2/usb5/5-2/5-2:1.0/ttyUSB0/tty/ttyUSB0
 */


    strncpy(buff, "", BUFF_SIZE - 1); // Erase buffer
		yield();

	} /* /while */
  close(fd);
}
/* vim: set ts=2 sw=2 sts=2 et:*/
