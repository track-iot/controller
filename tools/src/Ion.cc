/**  Ion Interface class implementation
 *
 * The device is composed of 4 antenna outpout,
 * using 4 radio set of sources.
 * A radio (Source) could be composed of one or many antenna (readpoint)
 * We ask one radio for reading, and it enable all antenna associated with it.
 * Source_0 could have Ant0, Ant1, Ant2, Ant3
 * Source_1 could have Ant0, Ant1
 * Source_3 could have Ant3
 * …
 */


#include "Ion.h"

using namespace::std;


bool Ion_s::add_new(string node, string id)
{
  Ion_s* s = new Ion_s;
  if(s == NULL)
    write_log("new ion NULL", ERR_TYPE);
  
  s->ion = new Ion(node, id);
//  s->ion->start();
  if(s->ion->is_disconnected())
   {
      free(s);
      return false;
   }
  else
   {
      s->next = next;
      next = s;
   }
  return true;
}

void Ion_s::remove_next()
{
  write_log("Deleting Ion", WARN_TYPE);
  Ion_s* s = next;
  if(s == NULL)
    write_log("new ion NULL in rm", ERR_TYPE);
  next = s->next;
  delete s->ion;
  delete s;
}

std::string Ion::get_addr()
{
  return address;
}

string Ion::get_uid()
{
  return uid;
}

Ion::Ion(std::string addr, string id): fd(NULL), active_antennae(0), disconnected(true)
{
    uid = id;
    address = addr;
  char* add;
  add = (char*) malloc((address.length() + 1) * sizeof(char));
  strcpy(add, address.c_str());

  /* Start a new connection with the CAENRFIDD server */
  if(caenrfid_open(CAENRFID_PORT_TCP, add, &handle) < 0)
  {
    write_log("cannot init caenrfid lib", ERR_TYPE);
  }
  else
  {
    disconnected = false;
    uint32_t power;
    get_power(&power);
    stringstream ss;
    ss << "Creating new ION, its default power is " << power << "mW";
    write_log(ss.str(), WARN_TYPE);
    init();
  }
  free(add);
}


Ion::~Ion()
{
    if(DEBUG > 2)
        write_log("Ion destructor");

  /* Close the connection */
  if(caenrfid_close(&handle) < 0)
    write_log("improper caenrfidlib closure!", ERR_TYPE);

}

/** This test each radio in order to enable them if a tag is read
 * (used to check from which port an antenna is connected).
 * read http://support.caenrfid.it/wiki/index.php/Caenrfidd_daemon#Theory_of_operation
 * to understand the difference between radio and antenna
 */
void Ion::init()
{

//bool Ion::do_inventory(const char* src)
//

  std::stringstream ss;

  strcpy(antennae[0], SOURCE_0);
  strcpy(antennae[1], SOURCE_1);
  strcpy(antennae[2], SOURCE_2);
  strcpy(antennae[3], SOURCE_3);

  ss << "Checking now which antenna appear to be connected: \n";

  for(int i = 0; i < MAX_ANTENNA; i++)
   {
      if(do_inventory(antennae[i]))
       {
          active_antennae++;
          ss << "Antenna #" << i +1 << " enabled" << std::endl;
       }
      else
       {
          strcpy(antennae[i], SOURCE_DISABLED);
          ss << "Antenna #" << i +1 << " disabled" << std::endl;
       }
   }

  write_log(ss.str());

}


Ion::Ion(Ion&)
{

  write_log("To implement, copy constructor, ion", WARN_TYPE);
}

/** Connect to the caenrfid ION reader, 
 * and do an inventory.
 * Report the number of tag read,
 * from which antenna,
 * the time spent.
 * It is recursivly doing it with all enabled antennae (see init func)
 * @param the DBhandler instance
 * @return a unique key in order to retrieve
 *  data from the database.
 * @TODO should probably check if the key is really unique (from DB)
 */
const char* Ion::do_inventory(DBhandler& db)
{

  int tags_read = 0;
  struct caenrfid_tag *tag;
  size_t size;
  char *str;
  int ret;
  boost::posix_time::ptime time;
  boost::posix_time::time_duration duration;
  double begin;
  double time_spent_ms;
  std::stringstream lol, logdb;

  if(active_antennae == 0)
    return "NO ANTENNA";

  /* Generate unique enventory number */
  char key[KEY_SIZE];

  gen_key(key, KEY_SIZE);

  lol << "\n ======================================================================" << endl;
  lol <<   " ========== Doing Inventory `" << key << "', find the following tags =========" << endl;


  for(int j = 0; j< MAX_ANTENNA; j++)
   {
      if(strcmp(antennae[j], SOURCE_DISABLED) == 0)
        continue;

      lol <<   " ========== Antenna: " << antennae[j] << endl;

    /* Do the inventory */
      time = boost::posix_time::microsec_clock::local_time();
      duration =  time.time_of_day();
      begin = duration.total_milliseconds();

      ret = caenrfid_inventory(&handle, antennae[j], &tag, &size);

      time = boost::posix_time::microsec_clock::local_time();
      duration =  time.time_of_day();
      time_spent_ms = duration.total_milliseconds() - begin;

      if (ret < 0) {
        write_log("cannot get data", ERR_TYPE, ret);
        return "FAIL";
      }

      /* Report results */
      for (int i = 0; i < size; i++) 
       {
          str = bin2hex(tag[i].id, tag[i].len);
          if (!str)
           {
              write_log("cannot allocate memory!", ERR_TYPE);
              return "FAIL";
           }
          /*
          printf("%.*s %.*s %.*s %d\n",
            tag[i].len * 2, str,
            CAENRFID_SOURCE_NAME_LEN, tag[i].source,
            CAENRFID_READPOINT_NAME_LEN, tag[i].readpoint,
            tag[i].type);
          */

          db.logTag(uid.c_str(), str, tag[i].readpoint, key); ///@TODO probably need to check duplicates (do we want to know from which antenna we could read this tag, or simply continue if already reads?
     
          lol << " " << str << " from antenna:"  << tag[i].readpoint << ". Tag type: " << tag[i].type << std::endl;

          free(str);
          tags_read++;
       }


      lol <<   " ========== " << size << " tags in: " << time_spent_ms << "ms " << endl;

      /* Free inventory data */
      logdb << key << ":" << tag[0].readpoint << ":Inventory done:" << size << " tags read";
      db.log(logdb.str(), WARN_TYPE, time_spent_ms);
      logdb.str("");

      free(tag);

   } // for MAX_ANTENNA

  write_log(lol.str(), WARN_TYPE);

  std::string kk(key);  
  return kk.c_str();
}


/** Do an inventory with all antennae in order to disable the 
 *   one not connected. There is no way to find that out by software
 *   the init do it by checking if a tag is read… 
 *   __YOU NEED__ to have a tag neer your antenna at boot time (init time)!
 * @return true if found a tag, false if not
 */ 
bool Ion::do_inventory(char* src)
{

  struct caenrfid_tag *tag;
  size_t size;
  int ret;
  bool enabled = false;

  ret = caenrfid_inventory(&handle, src, &tag, &size);
  if (ret < 0) {
    write_log("cannot get data", ERR_TYPE, ret);
    return false;
  }

  if (size > 0)
   {
      enabled = true;
   }

  free(tag);

  return enabled;
}

char Ion::nibble2hex(char c)
{
        switch (c) {
        case 0 ... 9:
                return '0' + c;

        case 0xa ... 0xf:
                return 'a' + (c - 10);
        }

        printf("got invalid data!");
        return '\0';
}

char *Ion::bin2hex(uint8_t *data, size_t len)
{
        char *str;
        int i;

        str = (char*)malloc(len * 2 + 1);
        if (!str)
                return NULL;

        for (i = 0; i < len; i++) {
                str[i * 2] = nibble2hex(data[i] >> 4);
                str[i * 2 + 1] = nibble2hex(data[i] & 0x0f);
        }
        str[i * 2] = '\0';

        return str;
}

void Ion::gen_key(char* k, int ii)
{
  srand(time(NULL));
  for(int i = 0; i < ii ; i++)
  {
    if(i%2)
      k[i] = rand() % ('Z' - 'A') + 'A' + 1;
    else
      k[i] = rand() % ('9' - '0') + '0' + 1;
  }
}

bool Ion::get_power(uint32_t* power)
{
  if(caenrfid_get_power(&handle, power) < 0)
  {
    write_log("cannot get power", ERR_TYPE);
    return false;
  }

  return true;
}


/** Change the emit power.
 * It cut the power value to the nearest possible one
 * The effective radiate power in mW ERP (Perp) is related to the 
 *   conducted RF power (Pw) provided at the reader's
 *   connector by the following formula:
 *   Pw = (Perp) / (10^([G−2.14−L]÷10)
 *   where G is the antenna gain expressed in dBi and L the cable attenuation expressed in dB.
 */
bool Ion::set_power(uint32_t power)
{
  if(caenrfid_set_power(&handle, power) < 0)
  {
    write_log("cannot set power", ERR_TYPE);
    return false;
  }
  if(DEBUG > 5) write_log("Set power");

  return true;
}


bool Ion::get_protocol(uint32_t* proto)
{
  if(caenrfid_get_protocol(&handle, proto) < 0)
  {
    write_log("cannot get protocol", ERR_TYPE);
    return false;
  }

  return true;

}


bool Ion::set_protocol(uint32_t proto)
{
  if(caenrfid_set_protocol(&handle, proto) < 0)
  {
    write_log("cannot set protocol", ERR_TYPE);
    return false;
  }
  if(DEBUG > 5) write_log("Set protocol");

  return true;

}


/** Using the gen2 inventory method (called “Slotted ALOHA” algorithm)
 * you can define the q value.
 * Recommended Q values:
 *   Estimated number of tags          Starting Q value
 *   1                                 0
 *   2                                 1
 *   3–6                               2
 *   7 – 15                            3
 *   16 – 30                           4
 *   30 – 50                           5
 *   50 – 100                          6
 *   100 – 200                         7
 */
bool Ion::get_g2_q(uint16_t* q)
{
  if(caenrfid_g2_get_q(&handle, q) < 0)
  {
    write_log("cannot get Gen2 q value", ERR_TYPE);
    return false;
  }

  return true;

}


bool Ion::set_g2_q(uint16_t q)
{
  if(caenrfid_g2_set_q(&handle, q) < 0)
  {
    write_log("cannot set Gen2 q value");
    return false;
  }
  if(DEBUG > 5) write_log("Set q");

  return true;

}


bool Ion::check_readpoint(char* source, char* antenna, uint16_t* value)
{
  if(caenrfid_check_readpoint(&handle, source, antenna, value) < 0)
  {
    write_log("cannot check readpoint", ERR_TYPE);
    return false;
  }

  return true;

}


bool Ion::add_readpoint(char* source, char* antenna)
{
  if(caenrfid_add_readpoint(&handle, source, antenna) < 0)
  {
    write_log("cannot add readpoint");
    return false;
  }

  return true;

}


bool Ion::remove_readpoint(char* source, char* antenna)
{
  if(caenrfid_remove_readpoint(&handle, source, antenna) < 0)
  {
    write_log("cannot remove readpoint", ERR_TYPE);
    return false;
  }

  return true;

}

bool Ion::is_disconnected()
{
  return disconnected;
}

/* vim: set ts=2 sw=2 sts=2 et:*/
