/** Generic functions definition
 *
 */

#ifndef GNRL_FUCT
#define GNRL_FUCT

#include "header.h"
#include "time.h"

using namespace std;

/** write_log handle log.
 * @params log, std::string, which is what you want to display
 * @params err_type, could either be 
 *  - INFO_TYPE, for stdout general display
 *  - WARN_TYPE, for bold stdout display
 *  - ERR_TYPE, for red stderr display.
 * @params err: if err exists, errno will be set and perror run.
 * @TODO creating a rotative log
 * 
 */
void write_log(std::string log, const char* err_type, int err)
{
  stringstream t;
  t.str("");
  
  if(DEBUG_TIME)
  {
    time_t now;
    struct tm* tt;

    time(&now); 
    tt = localtime(&now);
    /* 2012-01-11 10:12:43 */
    t <<  tt->tm_year + 1900 << "-";
    if(++tt->tm_mon < 10)
      t << "0";
    t << tt->tm_mon << "-";
    if(tt->tm_mday < 10)
      t << "0";
    t << tt->tm_mday;
    t << " ";
    if(tt->tm_hour < 10)
      t << "0";
    t << tt->tm_hour << ":";
    if(tt->tm_min < 10)
      t << "0";
    t << tt->tm_min << ":";
    if(tt->tm_sec < 10)
      t << "0";
    t << tt->tm_sec << " ";
  }

  if(!strcmp(err_type, ERR_TYPE))
  {
    cerr << t.str() << "\033[1;31m" << "[" << ERR_TYPE << "] ";    // red
    if(err){    
      errno = err;
      perror(log.c_str()); 
      cerr << "\033[0m\n"; // end of red 
    }
   else
     cerr << log << "\033[0m\n";    // end of red
  }else{
    cout << t.str();
    if(!strcmp(err_type, WARN_TYPE))
      cout << "\033[1;131m" << "[" << WARN_TYPE << "] " << log << "\033[0m\n";       // bold
    else
      cout << log << endl;                            // default
  }
}

#endif
/* vim: set ts=2 sw=2 sts=2 et:*/
