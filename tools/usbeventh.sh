#!/bin/sh
# see 80-rfidcontroller.rules to get some details
# this script is run by 80-rfidcontroller.rules each time
# a new usb device is connected or removed, sending device 
# attribut to the controller through named pipe.

REMOVAL='removing'

PIPE_NAME=/tmp/controller.p

if [[ ! -p $PIPE_NAME ]]
then
    echo "The controller is not running… Exiting"
    exit 1
fi

if [ -z "$3" ] && [ ! -z "$2" ] && [ "$1" == $REMOVAL ] # Only 2 args, with the first one maching removal.
then
    echo "$*" > $PIPE_NAME
# This one is called numerous times, cause it removes all parents. Last one, matching %p is the right one
elif [ ! -z "$7" ]   # Adding a device
then
    echo "$*" > $PIPE_NAME
elif [ ! -z "$2" ]   # Sending its attached device
then
    echo "$*" > $PIPE_NAME
fi

exit 0
