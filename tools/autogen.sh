#!/bin/sh

tree=(build m4 config archives)

for i in "${tree[@]}"
do
  if [ ! -d $i ]
  then
    mkdir -p $i
  fi
done

autoreconf --force --install -I config -I m4
