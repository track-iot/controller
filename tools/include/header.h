#ifndef HEADER_H
#define HEADER_H

#include <iostream>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <cstring>
#include <sys/stat.h>

/* Wide project includes */
#include "DBhandler.h"  /// This class instanciated as singleton handle database managment

#define DEBUG       6 	/// decrease to 0 to log minimum info. TODO: should be retreived from the cfg file
#define DEBUG_TIME  1
#define DEBUG_DB_QUERY  0
#define DEBUG_SOCK  0

#define QUIT_MSG      "please_quit_gently"

#define LOG_BUFF 100 ///\TODO change to be dynamic

/** Code wide macros
 * used for general purpose
 */

#define print_e(val, msg)\
    do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)

#define READER    "R"
#define TAG       "T"
#define SENSOR    "S"
#define ACTUATOR  "A"
#define ENDPOINT  "E"

enum eNetworkType {
  Zigbee,            // Through coordinator (should register() to it \TODO implement)
  Eth,
  Bluetooth,         // Through coordinator (should register() to it
  Serial
};

enum eRfidType {
  HF,
  UHF,
  SHF
};


/** The following functions are use to handle local logs
 * Never call them aside of DBhandler logs, as you will get duplicate from DBhandler
 * Passing errno as err argument will specify that an error occurs (calling perror)
 */

void write_log(std::string log, const char* err_type="II", int err=0);

#endif
/* vim: set ts=2 sw=2 sts=2 et:*/
