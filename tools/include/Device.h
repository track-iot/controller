/**  Device classes Definition
 *
 *
 *
 */


#ifndef _DEVICE_H
#define _DEVICE_H

#include "header.h"

class Device {
  protected:
    int id;
    int type;
    int adress;

  public:
    Device();
     Device(std::string); ///\Note, should use template as constructor inheritance is not yet implemented in gcc (c++0x standard)
     int getId();
     void send(const char*);
};


class Actuator: public Device {

};

class Tag: public Device {
  private:
    int right;

};

#endif
/* vim: set ts=2 sw=2 sts=2 et:*/
