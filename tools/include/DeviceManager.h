/**  DeviceManager classe definition
 *
 */

#ifndef _DEVICEMANAGER_H
#define _DEVICEMANAGER_H


#include <map>

#include "Pthread.h"
#include "Device.h"
#include "header.h"
#include "Redbee.h"
#include "Sensors.h"
#include "ID.h"
#include "Ion.h"
#include "SocketServer.h"
#include "Actuators.h"


#define ID_NOT_FOUND        "ID NOT FOUND"
#define ID_UNKNOWN          "ID UNKNOWN"
#define DEVICE_UNKNOWN      "ID UNKNOWN"
#define DEVICE_NOT_FOUND    "Device not found"
#define CMD_UNKNOW          "Unknown command"
#define NOT_FOUND           '\0'
#define NOT_FOUND_S         "\0"
#define DONE                "DONE"
#define FAILED              "FAILED"

/**
 *  * DON'T forget to add devices.<new>.next = NULL in DeviceManager::init
 */
typedef struct
{
  Redbee_s        redbee_list;  // This list hold all redbee instance
  ID_s            id_list;
  Sensors_s       sensors_list;
  Ion_s           ion_list;
  Actuators_s     actuators_list;
/* should add others here*/
}Devices_s;


class Device;  // Forward declaration

class DeviceManager: public virtual Pthread {
  private:
    std::string process_cmd(const char*);
    std::string adm_cmd(const char*, std::string, const char*);
    bool read_tags();
    void init();
    bool init_device(const char*);
    void add_device(const char*, const char* , const char*, const char*, 
                    const char*, const char*);
    void add_device(const char*, const char* , const char*, const char*, 
                    const char*, const char*, 
                    const char*, const char*, const char*);
    const char get_cmd_type(char*&);
    const char* get_next(char*&);
    const char* do_action(const char* action_id);
    void scenario_tag(const char*, const char*); // id tag, id reader
    void scenario_tag(const char*, std::string ); // id tag, id reader
    bool set_field_number(mysqlpp::StoreQueryResult, int&, int&, int&, int&, int&, int&, int&);
    bool set_field_number(mysqlpp::StoreQueryResult, int&, int&, int&, int&, int&, int&, int&, int&, int&, int&);
    bool check_usb_device(const char*, const char*, const char*, const char*);
    bool update_devices(const char*, const char*, const char*, const char*, const char*, const char*); 
  
    Devices_s devices;
    DBhandler* db;

  public:
    DeviceManager();
    virtual void run();
    bool newDevice(eNetworkType nt, std::string node, std::string idVendor, 
                   std::string idProduct, std::string serial, std::string manufacturer, 
                   std::string product, const char* dt=ENDPOINT);     
                    /// One device discovery get an event
    void removeDevice(eNetworkType nt, std::string node, const char* dt=ENDPOINT);  
                    /// One device discovery found a device removed
    ~DeviceManager();
/* call it with DeviceManager& dm = DeviceManager::getInstance(); */

};

#endif
/* vim: set ts=2 sw=2 sts=2 et:*/
