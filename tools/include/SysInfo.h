/**  SysInfo class definition
 *
 * This class provide some info about the system
 * or a specific process
 */

#ifndef SYSINFO_H
#define SYSINFO_H

#include <signal.h>
#include <fcntl.h>
#include "header.h"

typedef struct{
  std::string stime, cutime, priority, nice, num_threads, starttime, vsize, rt_priority;
  int thread_number;

}ps_info;

class SysInfo{
private:

protected:

public:
  SysInfo();
  ~SysInfo();

  std::string get_load();
  std::string get_uptime();
  ps_info get_ps_info(int);
};
#endif
/* vim: set ts=2 sw=2 sts=2 et:*/
