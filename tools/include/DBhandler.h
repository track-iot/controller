/**  DBhandler Class definition
 *
 *  @include mysql++
 *  This is a singleton, you are not allowed to call the constructor.
 *  Instead, get the running instance using DBhandler::getInstance()
 *  @todo take constant from cfg file! 
 *  @todo should not need to recompile if the host change!
 *  @defines row names, please keep lowecase
 */

#ifndef DB_HANDLER_H
#define DB_HANDLER_H

#include <mysql++/mysql++.h>
#include <string.h>

#include "header.h"
#include "DBdescription.h"

class DBhandler{

public:
    static DBhandler& getInstance();
    bool init();
    void log(const char*, const char* err=INFO_TYPE, double duration=0);
    void log(std::string, const char* err=INFO_TYPE, double duration=0);
    void logNotif(const char*);
    void logTag(const char*, const char*, const char*, const char *);    
    bool set_reader_state(const char*, const char*);
    bool get_reader_state(const char*);
    bool set_tag_reader(const char*, const char*);
    bool set_field(const char*, const char*, const char*, const char*);
    bool is_tag_known(const char*);
    bool add_tag(const char*, const char*, const char*);
    mysqlpp::StoreQueryResult get_known_devices(const char*);
    const char* get_field_from_id(const char*, const char*, const char*);
    const char* get_field_from_id(const char*, int, const char*);
    mysqlpp::StoreQueryResult get_action(const char* id);
    bool update_sensor_value(const char*, const char*);
    bool update_actuator(const char*, const char*);
    mysqlpp::StoreQueryResult get_scenario_tag(const char*);

   	// Destructor
   	virtual ~DBhandler();

private:
    mysqlpp::StoreQueryResult get_table_description(const char*);
    mysqlpp::Connection conn;
   	DBhandler();

    bool send_qry(std::string);
    mysqlpp::StoreQueryResult get_res_qry(std::string);

  };
#endif  // DB_HANDLER_H
/* vim: set ts=2 sw=2 sts=2 et:*/
