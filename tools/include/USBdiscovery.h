/**  USBdiscovery class definition
 *
 *
 *
 * This class is aimed to discover USB event in case we want to discover new USB decices (wireless router, RFID reader…)
 * Beware that it depends of the GLib library! Which is not so small…
 * \TODO:
 *   - be a standalone soft directly working with udev script and IPC?
 *   - make run() exitable, or memory efficient
 *
 */

#ifndef USBDISCOVERY_H
#define USBDISCOVERY_H

#include <signal.h>
#include <fcntl.h>
#include "header.h"
#include "DeviceManager.h"

#define BUFF_SIZE 514
#define PATH_PIPE "/tmp/controller.p"

#include "Pthread.h"

class USBdiscovery: public virtual Pthread{
private:
  std::string idVendor, idProduct, p, N, serial, manufacturer, product;
  DeviceManager* dm;

protected:
  void init();

public:
  USBdiscovery(DeviceManager*);
  ~USBdiscovery();
  virtual void run();
  bool splitData(std::string str);
};
#endif
/* vim: set ts=2 sw=2 sts=2 et:*/
