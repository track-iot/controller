/**  ID Interface class definition
 */

#ifndef ID_H
#define ID_H

#include "header.h"
#include "DBhandler.h"
#include <queue>
#include <time.h>
#include "Reader.h" 
#include <termios.h>
#include <fcntl.h>



#define SIZEOFRESPONSE  64
#define NULL_PORT "/dev/null"
#define MAX_TRY   3

#define ERR_NOT_RUNNING   "33"
#define ERR_DEVICE_CLOSED "66"
#define DEF_RES           "FFFF"
#define ERR_UID           -1
#define CHECKSUM_ERROR    "CKEE"


class ID : public Reader{
public:
//  ID();
  ID(std::string node, std::string);
  virtual ~ID();
  ID(ID&);
 
  virtual void run();
  std::string get_oldest_msg();
  bool is_running();
  std::string get_port();
  std::string get_uid();
  std::string get_tag_fifo();

protected:

  FILE *fd;
  std::string uid;
  std::queue<std::string> queue;
  char reponse[SIZEOFRESPONSE];
  std::string port;
  const char* get_data(const char*);
  bool verif_checksum(const char*);
  bool started;
  std::string escape_char(const char*);
  bool read(void);
private:
};


/* the list holding all ID instance */
class ID_s
{
  public:
    ID* id;
    ID_s* next;

    bool add_new(std::string, std::string);
    bool right_device(const char*, const char*, const char*);
    void remove_next();  
};


#endif 
/* vim: set ts=2 sw=2 sts=2 et:*/
