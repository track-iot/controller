/**  Database description
 *  @defines row names, please keep lowecase
 * @define db name, login
 * @define table names
 */

#ifndef DB_DESCR_H
#define DB_DESCR_H

// DB log  type

#define ERR_TYPE    "EE"
#define WARN_TYPE   "WW"
#define INFO_TYPE   "II"

//#define DB_HOST         "localhost"
#define DB_HOST         "157.159.100.218"
#define DB_PORT         3306
#define DB_NAME         "TrackIot"
#define DB_LOGIN        "root"
#define DB_PASSW        "talk123"

/* Tables wide fields */
#define DB_F_TYPE       "type"
#define DB_F_MSG        "text"
#define DB_F_LOCATION   "location"
#define DB_F_ACTION     "action"
#define DB_F_COMMENT    "comment"


/* Tables                               name                field      */
#define DB_T_LOG_SYS                    "Log_sys"
  #define DB_F_LOG_SYS_TYPE                                 DB_F_TYPE
  #define DB_F_LOG_SYS_MSG                                  DB_F_MSG
  #define DB_F_LOG_SYS_DURATION                             "duration_ms"
#define DB_T_LOG_TAG                    "Log_tag"
  #define DB_F_LOG_TAG_ID                                   "id_tag"
  #define DB_F_LOG_TAG_READER_ID                            "id_reader"
  #define DB_F_LOG_TAG_LOC                                  DB_F_LOCATION
  #define DB_F_LOG_TAG_ACTION                               DB_F_ACTION
#define DB_T_LOG_NOTIF                  "Notifications"
  #define DB_F_LOG_NOTIF_NOTIF                              "notifications"
#define DB_T_SENSORS                    "Sensors"
  #define DB_F_SENSORS_ID                                   "id_sensor"
  #define DB_F_SENSORS_VALUE                                "value"
  #define DB_F_SENSORS_NAME                                 "name"
  #define DB_F_SENSORS_TYPE                                 "type"
  #define DB_F_SENSORS_COMMENT                              DB_F_COMMENT
  #define DB_F_SENSORS_IFACE                                "interface"
  #define DB_F_SENSORS_IFACE_USB                                                "usb"
  #define DB_F_SENSORS_IFACE_XBEE                                               "xbee"
  #define DB_F_SENSORS_NODE                                 "node"
  #define DB_F_SENSORS_ADDRESS                              "adress_ip"
  #define DB_F_SENSORS_PRODUCT                              "product"
  #define DB_F_SENSORS_PRODUCT_ID                           "product_id"
  #define DB_F_SENSORS_VENDOR_ID                            "vendor_id"
  #define DB_F_SENSORS_SERIAL                               "serial"
#define DB_T_READERS                    "Readers"
  #define DB_F_READER_ID                                    "id"
  #define DB_F_READER_TYPE                                  "type"
  #define DB_F_READER_STATE                                 "state"
  #define DB_F_READER_STATE_ACTIVE                                              "on"
  #define DB_F_READER_STATE_INACTIVE                                            "off"
  #define DB_F_READER_IFACE                                 "interface"
  #define DB_F_READER_IFACE_USB                                                 DB_F_SENSORS_IFACE_USB
  #define DB_F_READER_IFACE_XBEE                                                DB_F_SENSORS_IFACE_XBEE
  #define DB_F_READER_ERROR                                 "errors"
  #define DB_F_READER_VENDOR                                "vendor_id"
  #define DB_F_READER_PRODUCT                               "product"
  #define DB_F_READER_PRODUCT_REDBEE                                            "Redbee"
  #define DB_F_READER_PRODUCT_INNOVATIONS                                       "ID-20"
  #define DB_F_READER_PRODUCT_CAENRFID_ION                                      "caenrfid"
  #define DB_F_READER_PRODUCT_ID                            "product_id"
  #define DB_F_READER_SERIAL                                "serial"
  #define DB_F_READER_MANUFACTURER                          "manufacturer"
  #define DB_F_READER_POWER                                 "power"
  #define DB_F_READER_COMMENT                               DB_F_COMMENT
  #define DB_F_READER_NODE                                  DB_F_SENSORS_NODE
  #define DB_F_READER_ADDRESS                               DB_F_SENSORS_ADDRESS
#define DB_T_TAGS                       "Tags"
  #define DB_F_TAGS_UID                                     "id"
  #define DB_F_TAGS_ID                                      "id_tag"
  #define DB_F_TAGS_READER_ID                               "id_reader"
  #define DB_F_TAGS_TECH                                    "tech"
  #define DB_F_TAGS_IS_FIXED                                "isfixed"
  #define DB_F_TAGS_GROUP                                   "group"
  #define DB_F_TAGS_COMMENT                                 DB_F_COMMENT
  #define DB_F_TAGS_LOC                                     DB_F_LOCATION
#define DB_T_ACTION                     "Command"
  #define DB_F_ACTION_ID                                    "id"
  #define DB_F_ACTION_DEVICE_TYPE                           "device_type"
  #define DB_F_ACTION_DEVICE_ID                             "device_id"
  #define DB_F_ACTION_CMD                                   "cmd"
#define DB_T_SC_T                       "Scenario_tag"
  #define DB_F_SC_T_UID                                     "id"
  #define DB_F_SC_T_UID_ACTION                              "uid_action_cmd"
  #define DB_F_SC_T_STATE                                   "state"
  #define DB_F_SC_T_STATE_ENABLE                                                "enable"
  #define DB_F_SC_T_STATE_DISABLE                                               "disable"
  #define DB_F_SC_T_UID_TAG                                 "uid_tag"
  #define DB_F_SC_T_UID_READER                              "uid_reader"
  #define DB_F_SC_T_LOC                                     "location"
  #define DB_F_SC_T_UID_SENSOR                              "uid_sensor"
  #define DB_F_SC_T_SENSOR_VALUE                                            "sensor_value"
#define DB_T_ACTUATORS                  "Actuators"
  #define DB_F_ACTUATORS_NAME                               "name"
  #define DB_F_ACTUATORS_ID                                 "id_actuator"
  #define DB_F_ACTUATORS_TYPE                               "type"
  #define DB_F_ACTUATORS_STATE                              "state"
  #define DB_F_ACTUATORS_STATE_ACTIVE                                           "on"
  #define DB_F_ACTUATORS_STATE_INACTIVE                                         "off"
  #define DB_F_ACTUATORS_IFACE                              "interface"
  #define DB_F_ACTUATORS_IFACE_USB                                              DB_F_SENSORS_IFACE_USB
  #define DB_F_ACTUATORS_IFACE_XBEE                                             DB_F_SENSORS_IFACE_XBEE
  #define DB_F_ACTUATORS_ERROR                              "error"
  #define DB_F_ACTUATORS_VENDOR                             "vendor_id"
  #define DB_F_ACTUATORS_PRODUCT                            "product"
  #define DB_F_ACTUATORS_PRODUCT_REDBEE                                         "Redbee"
  #define DB_F_ACTUATORS_PRODUCT_INNOVATIONS                                    "ID-20"
  #define DB_F_ACTUATORS_PRODUCT_CAENRFID_ION                                   "caenrfid"
  #define DB_F_ACTUATORS_PRODUCT_ID                         "product_id"
  #define DB_F_ACTUATORS_SERIAL                             "serial"
  #define DB_F_ACTUATORS_MANUFACTURER                       "manufacturer"
  #define DB_F_ACTUATORS_POWER                              "power"
  #define DB_F_ACTUATORS_COMMENT                            DB_F_COMMENT
  #define DB_F_ACTUATORS_NODE                               DB_F_SENSORS_NODE
  #define DB_F_ACTUATORS_ADDRESS                            DB_F_SENSORS_ADDRESS

#endif
/* vim: set ts=2 sw=2 sts=2 et:*/
